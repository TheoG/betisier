# Projet PHP - Bêtisier

Projet conçu en 2ème année d'IUT Informatique.

## Consignes de l'exercice
- Programmation Orientée Objet avec modèle MVC ;
- Gestion des droits d'accès (compte admin et compte utilisateur) ;
- Lister, modifier et supprimer :
	- Des citations
	- Des personnes
	- Des villes
- Vérifications de mots interdits, et validation de citation par les modérateurs ;
- Les élèves peuvent noter les citations ;
- Site valide W3C pour le HTML et le CSS