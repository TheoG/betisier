<?php

class Etudiant extends Personne {
    
    private $per_num;
    private $dep_num;
    private $div_num;
    
    public function __construct($valeurs = array()) {
        parent::remplir($valeurs);
        if (!empty($valeurs))
            $this->remplir($valeurs);
    }
    
    public function remplir($donnees) {
        
        foreach($donnees as $attribut => $valeur) {
            switch ($attribut) {
                case 'per_num' : 
                    $this->setPer_num($valeur);
                    break;
                case 'dep_num' :
                    $this->setDep_num($valeur);
                    break;
                case 'div_num' :
                    $this->setDiv_num($valeur);
                    break;
            }
        }
    }
    
    public function getPer_num() {
        return $this->per_num;
    }
    
    public function getDep_num() {
        return $this->dep_num;
    }
    
    public function getDiv_num() {
        return $this->div_num;
    }
    
    public function setPer_num($num) {
        if (is_numeric($num)) {
            if ($num > 0) {
                $this->per_num = $num;
            }
            else {
                echo "Le numéro doit être positif";
            }
        }
        else {
            echo "Le numéro doit être de type entier";
        }
    }
    
    public function setDep_num($num) {
        if (is_numeric($num)) {
            if ($num > 0) {
                $this->dep_num = $num;
            }
            else {
                echo "Le numéro doit être positif";
            }
        }
        else {
            echo "Le numéro doit être de type entier";
        }
    }
    
    public function setDiv_num($num) {
        if (is_numeric($num)) {
            if ($num > 0) {
                $this->div_num = $num;
            }
            else {
                echo "Le numéro doit être positif";
            }
        }
        else {
            echo "Le numéro doit être de type entier";
        }
    }
}

?>