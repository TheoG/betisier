<?php
class PersonneManager {

    private $db;
    
    
    public function __construct() {
        try {
            $this->db = new Mypdo();
        } catch (PDOException $e) {
            echo "Impossible de se connecter ".$e;
        }
    }
    
    public function getListPersonnes() {
    
        $listePersonnes = array();
        
        $req = $this->db->prepare('SELECT   per_num, 
                                            per_nom, 
                                            per_prenom 
                                    FROM personne 
                                    ORDER BY per_nom');
            
        $req->execute();
        
        while ($personne = $req->fetch(PDO::FETCH_OBJ)) {
            $listePersonnes[] = new Personne($personne);
        }
        
        $req->closeCursor();
        return $listePersonnes;
    }

    // Permet de récupérer le nombre de personnes présents dans la base
    public function getNbPersonnes() {
    
        $nbPersonnes;
        
        $req = $this->db->prepare('SELECT COUNT(*) as nb FROM personne');
        
        $req->execute();
        $nbPersonnes = $req->fetch(PDO::FETCH_ASSOC);
        
        $req->closeCursor();
        return $nbPersonnes['nb'];
    }
    
    // Permet de récupérer une personne en fonction de son per_num
    public function getPersonne($idPersonne) {
        $personne;
        
        $req = $this->db->prepare('SELECT   per_num, 
                                            per_nom, 
                                            per_prenom, 
                                            per_tel, 
                                            per_mail, 
                                            per_admin, 
                                            per_login,
                                            per_pwd
                                        FROM personne 
                                        WHERE per_num = :id');
        
        $req->bindValue(':id', $idPersonne, PDO::PARAM_INT);
        $req->execute();
        
        $personne = $req->fetch(PDO::FETCH_OBJ);
        
        $req->closeCursor();
        
        return $personne;
    }
    
    // Permet de récupérer une personne via son login
    public function getPersonnePerLogin($login) {
        $personne;
        
        $req = $this->db->prepare('SELECT   per_num, 
                                            per_nom, 
                                            per_prenom, 
                                            per_tel, 
                                            per_mail, 
                                            per_admin, 
                                            per_login,
                                            per_pwd
                                        FROM personne 
                                        WHERE per_login = :login');
        
        $req->bindValue(':login', $login, PDO::PARAM_STR);
        $req->execute();
        
        $personne = $req->fetch(PDO::FETCH_OBJ);
        
        $req->closeCursor();
        
        return $personne;
    }
    
    // Fonction retournant vrai si la personne est un étudiant, faux sinon
    public function isEtudiant($idPersonne) {
        $req = $this->db->prepare('SELECT per_num FROM personne WHERE EXISTS (SELECT per_num FROM etudiant WHERE etudiant.per_num = :id)');
        $req->bindValue(':id', $idPersonne, PDO::PARAM_INT);
        
        $req->execute();
        
        $personne = $req->fetch(PDO::FETCH_ASSOC);
        $req->closeCursor();
        
        return $personne;
    }
    
    // Fonction permettant de vérifier le login et le mot de passe d'une personne lors de la connexion
    public function checkLoginAndPassword($login, $passwd) {
        $req = $this->db->prepare('SELECT   
                                            per_num,
                                            per_nom,
                                            per_prenom,
                                            per_tel,
                                            per_mail,
                                            per_admin,
                                            per_login,
                                            per_pwd
                                            FROM personne 
                                            WHERE per_login = :login AND per_pwd = :per_pwd');
        
        $req->bindValue(':login', $login, PDO::PARAM_STR);
        $req->bindValue(':per_pwd', $passwd, PDO::PARAM_STR);
        $req->execute();
        
        $personne = $req->fetch(PDO::FETCH_OBJ);
        
        $req->closeCursor();
        
        return $personne;
    }
    
    public function loginExist($login) {
        $req = $this->db->prepare('SELECT
                                    per_login
                                    FROM personne
                                    WHERE per_login = :login');
        
        $req->bindValue(':login', $login);
        
        $req->execute();
        
        $res = $req->fetch(PDO::FETCH_ASSOC);
        
        $req->closeCursor();
        
        if (empty($res)) {
            return false;
        } else {
            return true;
        }
    }
    
    // Fonction permettant d'ajouter une personne
    public function addPersonne($valeurs = array()) {
        $req = $this->db->prepare('INSERT INTO personne(per_nom, per_prenom, per_tel, per_mail, per_admin, per_login, per_pwd) VALUES (
                                                            :per_nom,
                                                            :per_prenom,
                                                            :per_tel,
                                                            :per_mail,
                                                            :per_admin,
                                                            :per_login,
                                                            :per_pwd)');
        
        $req->bindValue(':per_nom', $valeurs->getPer_nom(), PDO::PARAM_STR);
        $req->bindValue(':per_prenom', $valeurs->getPer_prenom(), PDO::PARAM_STR);
        $req->bindValue(':per_tel', $valeurs->getPer_tel(), PDO::PARAM_STR);
        $req->bindValue(':per_mail', $valeurs->getPer_mail(), PDO::PARAM_STR);
        $req->bindValue(':per_admin', $valeurs->getPer_admin(), PDO::PARAM_STR);
        $req->bindValue(':per_login', $valeurs->getPer_login(), PDO::PARAM_STR);
        $req->bindValue(':per_pwd', $valeurs->getPer_pwd(), PDO::PARAM_STR);
        
        $req->execute();
        $req->closeCursor();
    }
    
    public function modifierPersonne($valeurs = array()) {
        $req = $this->db->prepare('UPDATE personne SET per_nom = :per_nom,
                                                        per_prenom = :per_prenom,
                                                        per_tel = :per_tel,
                                                        per_mail = :per_mail,
                                                        per_admin = :per_admin,
                                                        per_login = :per_login,
                                                        per_pwd = :per_pwd
                                                    WHERE per_num = :per_num');
        
        $req->bindValue(':per_nom', $valeurs->getPer_nom(), PDO::PARAM_STR);
        $req->bindValue(':per_prenom', $valeurs->getPer_prenom(), PDO::PARAM_STR);
        $req->bindValue(':per_tel', $valeurs->getPer_tel(), PDO::PARAM_STR);
        $req->bindValue(':per_mail', $valeurs->getPer_mail(), PDO::PARAM_STR);
        $req->bindValue(':per_admin', $valeurs->getPer_admin(), PDO::PARAM_STR);
        $req->bindValue(':per_login', $valeurs->getPer_login(), PDO::PARAM_STR);
        $req->bindValue(':per_pwd', $valeurs->getPer_pwd(), PDO::PARAM_STR);
        $req->bindValue(':per_num', $valeurs->getPer_num(), PDO::PARAM_INT);
        
        $req->execute();
        
        $req->closeCursor();
    }
    
    public function supprimerPersonne($personne) {
        $req = $this->db->prepare('DELETE FROM personne WHERE per_num = :per_num');
        
        $req->bindValue(":per_num", $personne->getPer_num(), PDO::PARAM_INT);
        
        $req->execute();
        
        $req->closeCursor();
    }
}

?>