<?php

class Ville {
    
    private $vil_num;
    private $vil_nom;
    
    public function __construct($valeurs = array()) {
        if (!empty($valeurs))
            $this->remplir($valeurs);
    }
    
    public function remplir($donnees) {
        
        foreach($donnees as $attribut => $valeur) {
            switch ($attribut) {
                case 'vil_num' : 
                    $this->setVilNum($valeur);
                    break;
                case 'vil_nom' :
                    $this->setVilNom($valeur);
                    break;
            }
        }
    
    }
    
    public function getVil_num() {
        return $this->vil_num;
    }
    
    public function getVil_nom() {
        return $this->vil_nom;
    }
    
    
    public function setVilNum($num) {
        if (is_numeric($num)) {
            if ($num > 0) {
                $this->vil_num = $num;
            }
            else {
                echo "Le numéro doit être positif";
            }
        }
        else {
            echo "Le numéro doit être de type entier";
        }
    }
    
    public function setVilNom($nom) {
        if (is_string($nom)) {
            $this->vil_nom = $nom;
        }
        else {
            echo "Le nom de la ville doit être une chaine de caractère";
        }
    }
}

?>