<?php

class Fonction {
    
    private $fon_num;
    private $fon_libelle;
    
    public function __construct($valeurs = array()) {
        if (!empty($valeurs))
            $this->remplir($valeurs);
    }
    
    public function remplir($donnees) {
        
        foreach($donnees as $attribut => $valeur) {
            switch ($attribut) {
                case 'fon_num' : 
                    $this->setFon_num($valeur);
                    break;
                case 'fon_libelle' :
                    $this->setFon_libelle($valeur);
                    break;
            }
        }
    }
    
    public function getFon_num() {
        return $this->fon_num;
    }
    
    public function getFon_libelle() {
        return $this->fon_libelle;
    }
    
    public function setFon_num($num) {
        if (is_numeric($num)) {
            if ($num > 0) {
                $this->fon_num = $num;
            }
            else {
                echo "Le numéro doit être positif";
            }
        }
        else {
            echo "Le numéro doit être de type entier";
        }
    }
    
    public function setFon_libelle($num) {
        if (is_string($num)) {
                $this->fon_libelle = $num;
            }
        else {
            echo "La fonction doit être une chaine de caracteres";
        }
    }
}

?>