<?php
class VilleManager {

    private $db;


    public function __construct() {
        try {
            $this->db = new Mypdo();
        } catch (PDOException $e) {
            echo "Impossible de se connecter ".$e;
        }
    }

    public function getListVille() {

        $listeVilles = array();

        $req = $this->db->prepare('SELECT vil_num, vil_nom FROM ville ORDER BY vil_nom');

        $req->execute();

        while ($ville = $req->fetch(PDO::FETCH_OBJ)) {
            $listeVilles[] = new Ville($ville);
        }

        $req->closeCursor();
        return $listeVilles;
    }

    public function getNbVilles() {

        $nbVille;

        $req = $this->db->prepare('SELECT COUNT(*) as nb FROM ville');

        $req->execute();
        $nbVille = $req->fetch(PDO::FETCH_ASSOC);

        $req->closeCursor();
        return $nbVille['nb'];
    }

    public function getVille($vil_num) {
        $ville;

        $req = $this->db->prepare('SELECT   vil_num,
                                            vil_nom
                                        FROM ville
                                        WHERE vil_num = :id');

        $req->bindValue(':id', $vil_num, PDO::PARAM_INT);
        $req->execute();
        $ville = $req->fetch(PDO::FETCH_OBJ);

        $req->closeCursor();

        return $ville;
    }

    public function addVille($nom) {
        $req = $this->db->prepare("INSERT INTO ville(vil_nom) VALUES (:nom)");

        $req->bindValue(':nom', $nom, PDO::PARAM_STR);
        $req->execute();

        $req->closeCursor();
    }
    
    public function supprimerVille($vil_num) {
        $req = $this->db->prepare('DELETE FROM ville WHERE vil_num = :vil_num');
        
        $req->bindValue(":vil_num", $vil_num, PDO::PARAM_INT);
        
        $req->execute();
        
        $req->closeCursor();
    }
    
    public function changerNomVille($id, $nom) {
        $req = $this->db->prepare('UPDATE ville SET vil_nom = :nom WHERE vil_num = :id');
        
        $req->bindValue(':id', $id, PDO::PARAM_INT);
        $req->bindValue(':nom', $nom, PDO::PARAM_STR);
        
        $req->execute();
        
        $req->closeCursor();
    }
}

?>
