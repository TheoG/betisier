<?php
class DepartementManager {

    private $db;
    
    public function __construct() {
        try {
            $this->db = new Mypdo();
        } catch (PDOException $e) {
            echo "Impossible de se connecter ".$e;
        }
    }
    
    public function getDepartement($dep_num) {
        $departement;
        
        $req = $this->db->prepare('SELECT   dep_num,
                                            dep_nom,
                                            vil_num
                                        FROM departement
                                        WHERE dep_num = :id');
        
        $req->bindValue(':id', $dep_num, PDO::PARAM_INT);
        $req->execute();    
        $departement = $req->fetch(PDO::FETCH_ASSOC);
        
        $req->closeCursor();
        
        return $departement;
    }
    
    // Fonction permettant de récupérer le département par un numéro de ville
    public function getDepartementVille($vil_num) {
        $departement;
        
        $req = $this->db->prepare('SELECT   dep_num,
                                            dep_nom,
                                            vil_num
                                        FROM departement
                                        WHERE vil_num = :vil_num');
        
        $req->bindValue(':vil_num', $vil_num, PDO::PARAM_INT);
        $req->execute();    
        $departement = $req->fetch(PDO::FETCH_OBJ);
        
        $req->closeCursor();
        
        return $departement;
    }
    
    public function getListDep() {

        $listeDep = array();

        $req = $this->db->prepare('SELECT dep_num, dep_nom, vil_num FROM departement ORDER BY dep_num');

        $req->execute();

        while ($departement = $req->fetch(PDO::FETCH_OBJ)) {
            $listeDep[] = new Departement($departement);
        }

        $req->closeCursor();
        return $listeDep;
    }
    
    // Permet de récupérer une liste de département qui sont dans une ville donnée
    public function getDepartementParVil_num($vil_num) {
        $listeDepartement = array();
        
        $req = $this->db->prepare('SELECT   dep_num, 
                                            dep_nom, 
                                            vil_num 
                                    FROM departement WHERE vil_num = :vil_num');
        
        $req->bindValue(':vil_num', $vil_num, PDO::PARAM_INT);
        
        $req->execute();
        
        while ($departement = $req->fetch(PDO::FETCH_OBJ)) {
            $listeDepartement[] = new Departement($departement);
        }
        
        $req->closeCursor();
        return $listeDepartement;
    }
    
    public function supprimerDepartement($departement) {
        $req = $this->db->prepare('DELETE FROM departement WHERE dep_num = :dep_num');
        
        $req->bindValue(":dep_num", $departement->getDep_num(), PDO::PARAM_INT);
        
        $req->execute();
        
        $req->closeCursor();
    }
}

?>