<?php
class SalarieManager extends PersonneManager {

    private $db;

    public function __construct() {
        parent::__construct();
        try {
            $this->db = new Mypdo();
        } catch (PDOException $e) {
            echo "Impossible de se connecter ".$e;
        }
    }

    public function getSalarie($idPersonne) {
        $personne;

        $req = $this->db->prepare('SELECT   p.per_num,
                                            sal_telprof,
                                            fon_num,
                                            per_nom,
                                            per_prenom,
                                            per_tel,
                                            per_mail,
                                            per_admin,
                                            per_login
                                        FROM salarie s JOIN personne p 
                                        ON s.per_num = p.per_num
                                        WHERE p.per_num = :id');

        $req->bindValue(':id', $idPersonne, PDO::PARAM_INT);
        $req->execute();

        $personne = $req->fetch(PDO::FETCH_OBJ);
        $req->closeCursor();

        return $personne;
    }


    public function addSalarie($valeurs = array()) {
        $req = $this->db->prepare('INSERT INTO salarie(per_num, sal_telprof, fon_num) VALUES (
                                                            :per_num,
                                                            :sal_telprof,
                                                            :fon_num)');

        $req->bindValue(':per_num', $valeurs->getPer_num(), PDO::PARAM_INT);
        $req->bindValue(':sal_telprof', $valeurs->getSal_telprof(), PDO::PARAM_INT);
        $req->bindValue(':fon_num', $valeurs->getFon_num(), PDO::PARAM_INT);

        $req->execute();
        $req->closeCursor();
    }

    public function getListSalarie() {
        $listeSalarie = array();

        $req = $this->db->prepare('SELECT   s.per_num, 
                                            per_nom, 
                                            per_prenom,
                                            per_tel,
                                            per_mail,
                                            per_admin,
                                            per_login,
                                            per_pwd,
                                            sal_telprof,
                                            fon_num
                                    FROM personne p
                                    JOIN salarie s ON p.per_num = s.per_num
                                    ORDER BY per_nom');

        $req->execute();

        while ($salarie = $req->fetch(PDO::FETCH_OBJ)) {
            $listeSalarie[] = new Salarie($salarie);
        }

        $req->closeCursor();
        return $listeSalarie;
    }
    
    public function modifierSalarie($valeurs = array()) {
        $req = $this->db->prepare('UPDATE salarie SET sal_telprof = :sal_telprof, fon_num = :fon_num WHERE per_num = :per_num');
        
        $req->bindValue(':per_num', $valeurs->getPer_num(), PDO::PARAM_INT);
        $req->bindValue(':sal_telprof', $valeurs->getSal_telprof(), PDO::PARAM_INT);
        $req->bindValue(':fon_num', $valeurs->getFon_num(), PDO::PARAM_INT);
        
        $req->execute();
        $req->closeCursor();
    }

    public function supprimerSalarie($salarie) {
        $req = $this->db->prepare('DELETE FROM salarie WHERE per_num = :per_num');
        
        $req->bindValue(":per_num", $salarie->getPer_num(), PDO::PARAM_INT);
        
        $req->execute();
        
        $req->closeCursor();
    }
    
}

?>