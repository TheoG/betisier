<?php
class FonctionManager {

    private $db;
    
    public function __construct() {
        try {
            $this->db = new Mypdo();
        } catch (PDOException $e) {
            echo "Impossible de se connecter ".$e;
        }
    }
    
    public function getFonction($id) {
        $personne;
        
        $req = $this->db->prepare('SELECT   fon_num,
                                            fon_libelle
                                        FROM fonction
                                        WHERE fon_num = :id');
        
        $req->bindValue(':id', $id, PDO::PARAM_INT);
        $req->execute();
        
        $personne = $req->fetch(PDO::FETCH_OBJ);
        $req->closeCursor();
        
        return $personne;
    }
    
        public function getListFonction() {

        $listeFct = array();

        $req = $this->db->prepare('SELECT fon_num, fon_libelle FROM fonction ORDER BY fon_num');

        $req->execute();

        while ($fonction = $req->fetch(PDO::FETCH_OBJ)) {
            $listeFct[] = new Fonction($fonction);
        }

        $req->closeCursor();
        return $listeFct;
    }
}

?>