<?php
class EtudiantManager extends PersonneManager {

    private $db;
    
    public function __construct() {
        parent::__construct();
        try {
            $this->db = new Mypdo();
        } catch (PDOException $e) {
            echo "Impossible de se connecter ".$e;
        }
    }
    
    public function getEtudiant($idPersonne) {
        $personne;
        
        $req = $this->db->prepare('SELECT   p.per_num,
                                            dep_num,
                                            div_num,
                                            per_nom,
                                            per_prenom,
                                            per_tel,
                                            per_mail,
                                            per_admin,
                                            per_login
                                        FROM etudiant e JOIN personne p 
                                        ON e.per_num = p.per_num
                                        WHERE p.per_num = :id');
        
        $req->bindValue(':id', $idPersonne, PDO::PARAM_INT);
        $req->execute();
        
        $personne = $req->fetch(PDO::FETCH_OBJ);
        $req->closeCursor();
        
        return $personne;
    }
    
    // Permet de récupérer un étudiant par son numéro de département
    public function getEtudiantParDepNum($dep_num) {
        $listeEtudiant = array();
        
        $req = $this->db->prepare('SELECT   per_num, 
                                            dep_num, 
                                            div_num 
                                    FROM etudiant WHERE dep_num = :dep_num');
        
        $req->bindValue(':dep_num', $dep_num, PDO::PARAM_INT);
        
        $req->execute();
        
        while ($etudiant = $req->fetch(PDO::FETCH_OBJ)) {
            $listeEtudiant[] = new Etudiant($etudiant);
        }
        
        $req->closeCursor();
        return $listeEtudiant;
    }
    
    // Ajout d'étudiant
    public function addEtudiant($valeurs = array()) {
        $req = $this->db->prepare('INSERT INTO etudiant(per_num, dep_num, div_num) VALUES (
                                                            :per_num,
                                                            :dep_num,
                                                            :div_num)');
        
        $req->bindValue(':per_num', $valeurs->getPer_num(), PDO::PARAM_INT);
        $req->bindValue(':dep_num', $valeurs->getDep_num(), PDO::PARAM_INT);
        $req->bindValue(':div_num', $valeurs->getDiv_num(), PDO::PARAM_INT);
        
        $req->execute();
        $req->closeCursor();
    }
    
    // Modif un étudiant
    public function modifierEtudiant($valeurs = array()) {
        $req = $this->db->prepare('UPDATE etudiant SET dep_num = :dep_num, div_num = :div_num WHERE per_num = :per_num');
        
        $req->bindValue(':per_num', $valeurs->getPer_num(), PDO::PARAM_INT);
        $req->bindValue(':dep_num', $valeurs->getDep_num(), PDO::PARAM_INT);
        $req->bindValue(':div_num', $valeurs->getDiv_num(), PDO::PARAM_INT);
        
        $req->execute();
        $req->closeCursor();
    }
    
    // Delete un étudiant
    public function supprimerEtudiant($etudiant) {
        $req = $this->db->prepare('DELETE FROM etudiant WHERE per_num = :per_num');
        
        $req->bindValue(":per_num", $etudiant->getPer_num(), PDO::PARAM_INT);

        $req->execute();
        
        $req->closeCursor();
    }
}

?>