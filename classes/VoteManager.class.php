<?php
class VoteManager {

    private $db;
    
    public function __construct() {
        try {
            $this->db = new Mypdo();
        } catch (PDOException $e) {
            echo "Impossible de se connecter ".$e;
        }
    }
    
    // Permet de récupérer la moyenne des notes pour une citation passée en paramètre
    // -1 est renvoyé dans le cas ou la citation n'a pas pas été encore notée
    public function getMoyenne($id) {
           $moyenne;
        
            $req = $this->db->prepare('SELECT AVG(vot_valeur) as moy FROM vote WHERE cit_num = :id');
        
            $req->bindValue(':id', $id, PDO::PARAM_INT);
            $req->execute();
        
            $moyenne = $req->fetch(PDO::FETCH_ASSOC);
            
            $req->closeCursor();
        
            if ($moyenne['moy'] == null) {
                return -1;
            } else {
                return $moyenne['moy'];
            }
    }
    
    // Permet de récupérer l'ensemble des votes d'une personne
    public function getVoteParPerNum($per_num) {
        $listeVote = array();
        
        $req = $this->db->prepare('SELECT   cit_num, 
                                            per_num, 
                                            vot_valeur 
                                    FROM vote ');
            
        $req->execute();
        
        while ($vote = $req->fetch(PDO::FETCH_OBJ)) {
            $listeVote[] = new Vote($vote);
        }
        
        $req->closeCursor();
        return $listeVote;
    }
    
    // Permet de savoir si une personne à noté une citation donnée
    public function aVote($cit_num, $per_num) {
        $req = $this->db->prepare('SELECT cit_num, per_num, vot_valeur 
                                    FROM vote 
                                    WHERE cit_num = :cit_num
                                    AND per_num = :per_num');
        
        $req->bindValue(':cit_num', $cit_num, PDO::PARAM_INT);
        $req->bindValue(':per_num', $per_num, PDO::PARAM_INT);
        
        $req->execute();
        $res = $req->fetch(PDO::FETCH_OBJ);
            
        $req->closeCursor();
        
        return ($res);
    }
    
    public function voter($per_num, $cit_num, $vot_valeur) {
        $req = $this->db->prepare('INSERT INTO vote(cit_num, per_num, vot_valeur) VALUES (
                                                                                    :cit_num,
                                                                                    :per_num,
                                                                                    :vot_valeur)');
        
        $req->bindValue(':cit_num', $cit_num, PDO::PARAM_INT);
        $req->bindValue(':per_num', $per_num, PDO::PARAM_INT);
        $req->bindValue(':vot_valeur', $vot_valeur, PDO::PARAM_INT);
        
        $req->execute();
        
        $req->closeCursor();
    }
    
    
    // Fonction permettant de supprimer tous les votes d'une citation donnée
    public function supprimerVoteParCitation($cit_num) {
        $req = $this->db->prepare('DELETE FROM vote WHERE cit_num = :cit_num');
        
        $req->bindValue(":cit_num", $cit_num, PDO::PARAM_INT);
        
        $req->execute();
        
        $req->closeCursor();
    }
    
    public function supprimerVote($vote) {
        $req = $this->db->prepare('DELETE FROM vote WHERE cit_num = :cit_num');
        
        $req->bindValue(":cit_num", $vote->getCit_num(), PDO::PARAM_INT);
        
        $req->execute();
        
        $req->closeCursor();
    }
    
    public function supprimerVoteParPersonne($personne) {
        $req = $this->db->prepare('DELETE FROM vote WHERE per_num = :per_num');
        
        $req->bindValue(":per_num", $personne->getPer_num(), PDO::PARAM_INT);
        
        $req->execute();
        
        $req->closeCursor();
    }
}

?>