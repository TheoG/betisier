<?php

class Departement {
    
    private $dep_num;
    private $dep_nom;
    private $vil_num; 
    
    public function __construct($valeurs = array()) {
        if (!empty($valeurs))
            $this->remplir($valeurs);
    }
    
    public function remplir($donnees) {
        
        foreach($donnees as $attribut => $valeur) {
            switch ($attribut) {
                case 'dep_num' : 
                    $this->setDep_num($valeur);
                    break;
                case 'dep_nom' :
                    $this->setDep_nom($valeur);
                    break;
                case 'vil_num' :
                    $this->setVil_num($valeur);
                    break;
            }
        }
    
    }
    
    public function getDep_num() {
        return $this->dep_num;
    }
    
    public function getDep_nom() {
        return $this->dep_nom;
    }
    
    public function getVil_num() {
        return $this->vil_num;
    }
    
    public function setDep_num($num) {
        if (is_numeric($num)) {
            if ($num > 0) {
                $this->dep_num = $num;
            }
            else {
                echo "Le numéro doit être positif";
            }
        }
        else {
            echo "Le numéro doit être de type entier";
        }
    }

    public function setDep_nom($nom) {
        if (is_string($nom)) {
            $this->dep_nom = $nom;
        }
        else {
            echo "Le nom du département doit être une chaine de caractère";
        }
    }
    
    public function setVil_num($num) {
        if (is_numeric($num)) {
            if ($num > 0) {
                $this->vil_num = $num;
            }
            else {
                echo "Le numéro doit être positif";
            }
        }
        else {
            echo "Le numéro doit être de type entier";
        }
    }
}

?>