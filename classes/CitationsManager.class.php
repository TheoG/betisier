<?php
class CitationsManager {

    private $db;

    
    public function __construct() {
        try {
            $this->db = new Mypdo();
        } catch (PDOException $e) {
            echo "Impossible de se connecter ".$e;
        }
    }
    
    // Récupère toutes les citations valides
    public function getListCitation() {
    
        $listeCitations = array();
        
        $req = $this->db->prepare('SELECT   cit_num, 
                                            per_num,
                                            per_num_valide,
                                            per_num_etu,
                                            cit_libelle,
                                            cit_valide,
                                            cit_date,
                                            cit_date_valide,
                                            cit_date_depo
                                    FROM citation 
                                    WHERE cit_date_valide IS NOT NULL
                                    AND cit_valide = 1
									GROUP BY cit_num
                                    ORDER BY cit_date');
            
        $req->execute();
        
        while ($citation = $req->fetch(PDO::FETCH_OBJ)) {
            $listeCitations[] = new Citations($citation);
        }
        
        $req->closeCursor();
        return $listeCitations;
    }
    
    public function getListCitationNonConnecte() {
        $listeCitations = array();
        
        $req = $this->db->prepare('SELECT   cit_num, 
                                            per_num,
                                            per_num_valide,
                                            per_num_etu,
                                            cit_libelle,
                                            cit_valide,
                                            cit_date,
                                            cit_date_valide,
                                            cit_date_depo
                                    FROM citation 
                                    WHERE cit_date_valide IS NOT NULL
                                    AND cit_valide = 1
									GROUP BY cit_num
                                    ORDER BY cit_date
                                    LIMIT 2');
            
        $req->execute();
        
        while ($citation = $req->fetch(PDO::FETCH_OBJ)) {
            $listeCitations[] = new Citations($citation);
        }
        
        $req->closeCursor();
        return $listeCitations;
    }
    
    // Permet de récupérer les citations par élèves les ayant postées
    public function getListCitationParIdEtu($id) {
        $listeCitations = array();
        
        $req = $this->db->prepare('SELECT   cit_num, 
                                            per_num,
                                            per_num_valide,
                                            per_num_etu,
                                            cit_libelle,
                                            cit_valide,
                                            cit_date,
                                            cit_date_valide,
                                            cit_date_depo
                                    FROM citation
                                    WHERE per_num_etu = :id
									GROUP BY cit_num
                                    ORDER BY cit_date');
        
        $req->bindValue(":id", $id, PDO::PARAM_INT);
        
        $req->execute();
        
        while ($citation = $req->fetch(PDO::FETCH_OBJ)) {
            $listeCitations[] = new Citations($citation);
        }
        
        $req->closeCursor();
        return $listeCitations;
    }
    
    // Permet de récupérer la liste des citations validées par des étudiants (cas possible dans le jeu d'essai de la base)
    public function getListCitationParIdEtuValide($id) {
        $listeCitations = array();
        
        $req = $this->db->prepare('SELECT   cit_num, 
                                            per_num,
                                            per_num_valide,
                                            per_num_etu,
                                            cit_libelle,
                                            cit_valide,
                                            cit_date,
                                            cit_date_valide,
                                            cit_date_depo
                                    FROM citation
                                    WHERE per_num_valide = :id
									GROUP BY cit_num
                                    ORDER BY cit_date');
        
        $req->bindValue(":id", $id, PDO::PARAM_INT);
        
        $req->execute();
        
        while ($citation = $req->fetch(PDO::FETCH_OBJ)) {
            $listeCitations[] = new Citations($citation);
        }
        
        $req->closeCursor();
        return $listeCitations;
    }
    
    // Permet de récupérer la liste des citations par personnel à l'origine de la citation, ou qu'il a validé
    public function getListCitationParIdPerso($id) {
        $listeCitations = array();
        
        $req = $this->db->prepare('SELECT   cit_num, 
                                            per_num,
                                            per_num_valide,
                                            per_num_etu,
                                            cit_libelle,
                                            cit_valide,
                                            cit_date,
                                            cit_date_valide,
                                            cit_date_depo
                                    FROM citation
                                    WHERE per_num = :id
                                    OR per_num_valide = :id
									GROUP BY cit_num
                                    ORDER BY cit_date');
        
        $req->bindValue(":id", $id, PDO::PARAM_INT);
        
        $req->execute();
        
        while ($citation = $req->fetch(PDO::FETCH_OBJ)) {
            $listeCitations[] = new Citations($citation);
        }
        
        $req->closeCursor();
        return $listeCitations;
    }
    
    // Permet de récupérer la liste des citations invalides
    public function getListCitationInvalide() {
        $listeCitations = array();
        
        $req = $this->db->prepare('SELECT   cit_num, 
                                            per_num,
                                            per_num_valide,
                                            per_num_etu,
                                            cit_libelle,
                                            cit_valide,
                                            cit_date,
                                            cit_date_valide,
                                            cit_date_depo
                                    FROM citation 
                                    WHERE cit_date_valide IS NULL
                                    OR cit_valide = 0
									GROUP BY cit_num
                                    ORDER BY cit_date');
            
        $req->execute();
        
        while ($citation = $req->fetch(PDO::FETCH_OBJ)) {
            $listeCitations[] = new Citations($citation);
        }
        
        $req->closeCursor();
        return $listeCitations;
    }
    
    // Permet de récupérer le nombre de citations invalides
    public function getNbCitationsInvalides() {
        $nbCitation;

        $req = $this->db->prepare('SELECT COUNT(*) as nb FROM citation WHERE cit_valide = 0 OR cit_date_valide IS NULL');

        $req->execute();
        $nbCitation = $req->fetch(PDO::FETCH_ASSOC);

        $req->closeCursor();
        
        return $nbCitation['nb'];
    }
    
    // Permet de récupérer la meilleure citation (au niveau de la notation de la table vote)
    public function getBestCitation() {
        $citation;
        
        $req = $this->db->prepare('SELECT c.cit_num, 
                                            c.per_num, 
                                            c.per_num_etu, 
                                            c.per_num_valide, 
                                            c.cit_libelle, 
                                            c.cit_valide, 
                                            c.cit_date, 
                                            c.cit_date_depo, 
                                            c.cit_date_valide 
                                            FROM citation c JOIN vote v 
                                            ON v.cit_num=c.cit_num 
                                            WHERE cit_valide = 1 
                                            AND cit_date_valide IS NOT NULL 
                                            GROUP BY c.cit_num 
                                            HAVING AVG(vot_valeur) >=ALL 
                                                (SELECT AVG(vot_valeur) as Moyenne FROM citation c 
                                                JOIN vote v ON v.cit_num = c.cit_num 
                                                WHERE cit_valide = 1 
                                                AND cit_date_valide IS NOT NULL 
                                                GROUP BY c.cit_num )');
        
        $req->execute();
        $citation = new Citations($req->fetch(PDO::FETCH_OBJ));

        $req->closeCursor();
        
        return $citation;
    }
    
    // Permet de récupérer la dernière citation en date
    public function getLastCitation() {
        $citation;
        
        $req = $this->db->prepare('SELECT cit_num, 
                                            per_num, 
                                            per_num_etu, 
                                            per_num_valide, 
                                            cit_libelle, 
                                            cit_valide, 
                                            cit_date, 
                                            cit_date_depo, 
                                            cit_date_valide 
                                            FROM citation c 
                                            WHERE cit_valide = 1 
                                            AND cit_date_valide IS NOT NULL 
                                            GROUP BY c.cit_num
                                            HAVING cit_date >= ALL (
                                                SELECT cit_date FROM citation
                                                WHERE cit_valide = 1
                                                AND cit_date_valide IS NOT NULL
                                            )');
        
        $req->execute();
        $citation = new Citations($req->fetch(PDO::FETCH_OBJ));

        $req->closeCursor();
        
        return $citation;
    }
    
    // Permet de valider une citation (on y ajoute l'id du validateur, on passe cit_valid à 1 et la date à la date du jour)
    public function valider($id, $per_num) {
        $req = $this->db->prepare('UPDATE citation SET per_num_valide = :per_num, cit_valide = 1, cit_date_valide = :date WHERE cit_num = :cit_num');
        
        
        $date = new DateTime('now');
        $dateStr = $date->format('Y-m-d H:i:s');
        
        $req->bindValue(':per_num', $per_num, PDO::PARAM_INT);
        $req->bindValue(':date', $dateStr, PDO::PARAM_STR);
        $req->bindValue(':cit_num', $id, PDO::PARAM_INT);
        
        $retour = $req->execute();
        
        $req->closeCursor();
        
        return $retour;
    }
    
    public function getNbCitations() {
    
        $nbCitations;
        
        $req = $this->db->prepare('SELECT COUNT(*) as nb FROM citation WHERE cit_valide = 1 AND cit_date_valide is not null');
        
        $req->execute();
        $nbCitations = $req->fetch(PDO::FETCH_ASSOC);
        
        $req->closeCursor();
        return $nbCitations['nb'];
    }
    
    // Fonction d'ajout d'une citation
    public function addCitation($valeurs) {
        $req = $this->db->prepare('INSERT INTO citation( per_num,
                                                        per_num_valide,
                                                        per_num_etu,
                                                        cit_libelle,
                                                        cit_date,
                                                        cit_valide,
                                                        cit_date_valide,
                                                        cit_date_depo) VALUES (
                                                            :per_num,
                                                            :per_num_valide,
                                                            :per_num_etu,
                                                            :cit_libelle,
                                                            :cit_date,
                                                            :cit_valide,
                                                            :cit_date_valide,
                                                            :cit_date_depo
                                                        )');
        
        
        
        $req->bindValue(':per_num', $valeurs->getPer_num(), PDO::PARAM_INT);
        $req->bindValue(':per_num_valide', $valeurs->getPer_num_valide(), PDO::PARAM_INT);
        $req->bindValue(':per_num_etu', $valeurs->getPer_num_etu(), PDO::PARAM_INT);
        $req->bindValue(':cit_libelle', $valeurs->getCit_libelle(), PDO::PARAM_STR);
        $req->bindValue(':cit_date', $valeurs->getCit_date(), PDO::PARAM_STR);
        $req->bindValue(':cit_valide', $valeurs->getCit_valide(), PDO::PARAM_INT);
        $req->bindValue(':cit_date_valide', $valeurs->getCit_date_valide(), PDO::PARAM_STR);
        $req->bindValue(':cit_date_depo', $valeurs->getCit_date_depo(), PDO::PARAM_STR);
        
        $req->execute();
        
        $req->closeCursor();
    }
    
    // Fonction de recherche d'une citation
    // La variable $recherche correspond au texte saisi lors de la recherche
    // Les autres premières variables correspondent à des "booleens" qui permettent de savoir si l'on doit 
    // effectuer la recherche sur tel ou tel champs.
    // Dans le cas d'une recherche sans selection de différents critères, il suffirait de retirer ces variables
    // et de faire la recherche avec l'ensemble de la requette (et non la décomposé comme ci-dessous)
    public function rechercher($enseignant, $date, $note, $libelle, $recherche) {
        $listeCitations = array();
        
        $requette = 'SELECT   c.cit_num, 
                                            c.per_num,
                                            per_num_valide,
                                            per_num_etu,
                                            cit_libelle,
                                            cit_valide,
                                            cit_date,
                                            cit_date_valide,
                                            cit_date_depo
                                    FROM citation c 
                                    WHERE cit_date_valide IS NOT NULL
                                    AND cit_valide = 1';
        
        if (!empty($enseignant)) {
            $requette = $requette. ' AND c.per_num IN (SELECT s.per_num FROM salarie s
                                        JOIN personne p ON p.per_num = s.per_num AND CONCAT(p.per_nom, \' \', p.per_prenom) LIKE :enseignant)';
        }
    
        
        if (!empty($date)) {
            if (!empty($enseignant)) {
                $requette = $requette. ' OR cit_date LIKE :cit_date';
            }
            else {
                $requette = $requette. ' AND cit_date LIKE :cit_date';
            }
        }
        
        if (!empty($note)) {
            if (!empty($enseignant) || !empty($date)) {
                $requette = $requette. ' OR c.cit_num IN (SELECT cit_num FROM vote GROUP BY cit_num HAVING TRUNCATE(AVG(vot_valeur), 1) LIKE :moyenne)';
            }
            else {
                $requette = $requette. ' AND c.cit_num IN (SELECT cit_num FROM vote GROUP BY cit_num HAVING TRUNCATE(AVG(vot_valeur), 1) LIKE :moyenne)';
            }
        }
        
        if (!empty($libelle)) {
            if (!empty($enseignant) || !empty($date) ||!empty($note)) {
                $requette = $requette. ' OR c.cit_libelle LIKE :libelle';
            }
            else {
                $requette = $requette. ' AND c.cit_libelle LIKE :libelle';
            }
        }
        
        $requette = $requette. ' GROUP BY c.cit_num';
        
        $requette = $requette. ' ORDER BY cit_date';
        
        $req = $this->db->prepare($requette);
        
        if (!empty($enseignant)) {
            $req->bindValue(':enseignant', '%'.$recherche.'%', PDO::PARAM_STR);
        }
        
        if (!empty($date)) {
                $req->bindValue(':cit_date', '%'.$recherche.'%', PDO::PARAM_STR);
        }
        
        if(!empty($note)) {
            $req->bindValue(':moyenne', $recherche.'%', PDO::PARAM_INT);    
        }
        
        if(!empty($libelle)) {
            $req->bindValue(':libelle', '%'.$recherche.'%', PDO::PARAM_STR);    
        }
        
        $req->execute();
        
        while ($citation = $req->fetch(PDO::FETCH_OBJ)) {
            $listeCitations[] = new Citations($citation);
        }
        
        $req->closeCursor();
        return $listeCitations;
    }
    
    // Fonction permettant de supprimer une citation par son numéro
    public function supprimerCitationParNum($cit_num) {
        $req = $this->db->prepare('DELETE FROM citation WHERE cit_num = :cit_num');
        
        $req->bindValue(":cit_num", $cit_num, PDO::PARAM_INT);
        
        $req->execute();
        
        $req->closeCursor();
    }
    
    public function supprimerCitation($citation) {
        $req = $this->db->prepare('DELETE FROM citation WHERE cit_num = :cit_num');
        
        $req->bindValue(":cit_num", $citation->getCit_num(), PDO::PARAM_INT);
        
        $req->execute();
        
        $req->closeCursor();
    }

    // Fonction permettant de supprimer une citation par le num d'une personne (tous champs confondus)
    public function supprimerCitationParPerNum($per_num) {
        $req = $this->db->prepare('DELETE FROM citation WHERE per_num = :per_num OR per_num_valide = :per_num OR per_num_etu = :per_num');
        
        $req->bindValue(":per_num", $per_num, PDO::PARAM_INT);
        
        $req->execute();
        
        $req->closeCursor();
    }
}

?>