<?php

class Vote {
    
    private $cit_num;
    private $per_num;
    private $vot_valeur;
    
    public function __construct($valeurs = array()) {
        if (!empty($valeurs))
            $this->remplir($valeurs);
    }
    
    public function remplir($donnees) {
        
        foreach($donnees as $attribut => $valeur) {
            switch ($attribut) {
                case 'cit_num' : 
                    $this->setCit_num($valeur);
                    break;
                case 'per_num' :
                    $this->setPer_num($valeur);
                    break;
                case 'vot_valeur' :
                    $this->setVot_valeur($valeur);
                    break;
            }
        }
    
    }
    
    public function getCit_num() {
        return $this->cit_num;
    }
    
    public function getPer_num() {
        return $this->per_num;
    }
    
    public function getVot_valeur() {
        return $this->vot_valeur;
    }
    
    public function setPer_num($num) {
        if (is_numeric($num)) {
            if ($num > 0) {
                $this->per_num = $num;
            }
            else {
                echo "Le numéro doit être positif";
            }
        }
        else {
            echo "Le numéro doit être de type entier";
        }
    }

    public function setCit_num($num) {
        if (is_numeric($num)) {
            if ($num > 0) {
                $this->cit_num = $num;
            }
            else {
                echo "Le numéro doit être positif";
            }
        }
        else {
            echo "Le numéro doit être de type entier";
        }
    }
    
    public function setVot_valeur($num) {
        if (is_numeric($num)) {
            if ($num > 0) {
                $this->vot_valeur = $num;
            }
            else {
                echo "Le numéro doit être positif";
            }
        }
        else {
            echo "Le numéro doit être de type entier";
        }
    }
}

?>