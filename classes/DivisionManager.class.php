<?php
class DivisionManager {

    private $db;
    
    public function __construct() {
        try {
            $this->db = new Mypdo();
        } catch (PDOException $e) {
            echo "Impossible de se connecter ".$e;
        }
    }
    
    
    
    public function getDivision($div_num) {
        $division;
        
        $req = $this->db->prepare('SELECT   div_num,
                                            div_nom
                                        FROM division
                                        WHERE div_num = :id');
        
        $req->bindValue(':id', $div_num, PDO::PARAM_INT);
        $req->execute();    
        $division = $req->fetch(PDO::FETCH_ASSOC);
        
        $req->closeCursor();
        
        return $division;
    }
    
    
    public function getListDiv() {

        $listeDiv = array();

        $req = $this->db->prepare('SELECT div_num, div_nom FROM division ORDER BY div_num');

        $req->execute();

        while ($division = $req->fetch(PDO::FETCH_OBJ)) {
            $listeDiv[] = new Division($division);
        }

        $req->closeCursor();
        return $listeDiv;
    }
}

?>