<?php

class Salarie extends Personne {
    
    private $per_num;
    private $sal_telProf;
    private $fon_num;
    
    public function __construct($valeurs = array()) {
        parent::remplir($valeurs);
        if (!empty($valeurs))
            $this->remplir($valeurs);
    }
    
    public function remplir($donnees) {
        
        foreach($donnees as $attribut => $valeur) {
            switch ($attribut) {
                case 'per_num' : 
                    $this->setPer_num($valeur);
                    break;
                case 'sal_telprof' :
                    $this->setSal_telProf($valeur);
                    break;
                case 'fon_num' :
                    $this->setFon_num($valeur);
                    break;
            }
        }
    }
    
    public function getPer_num() {
        return $this->per_num;
    }
    
    public function getSal_telProf() {
        return $this->sal_telProf;
    }
    
    public function getFon_num() {
        return $this->fon_num;
    }
    
    public function setPer_num($num) {
        if (is_numeric($num)) {
            if ($num > 0) {
                $this->per_num = $num;
            }
            else {
                echo "Le numéro doit être positif";
            }
        }
        else {
            echo "Le numéro doit être de type entier";
        }
    }
    
    public function setSal_telProf($num) {
        if (is_string($num)) {
                $this->sal_telProf = $num;
            }
        else {
            echo "Le numéro doit être une chaine de caracteres";
        }
    }
    
    public function setFon_num($num) {
        if (is_numeric($num)) {
            if ($num > 0) {
                $this->fon_num = $num;
            }
            else {
                echo "Le numéro doit être positif";
            }
        }
        else {
            echo "Le numéro doit être de type entier";
        }
    }
}

?>