<?php
class MotManager {

    private $db;

    public function __construct() {
        try {
            $this->db = new Mypdo();
        } catch (PDOException $e) {
            echo "Impossible de se connecter ".$e;
        }
    }
    
    // Retourne vrai si le mot passé en paramètre est interdit, faux sinon
    public function isMotInterdit($mot) {
        $req = $this->db->prepare('SELECT mot_interdit FROM mot
                                    WHERE MATCH(mot_interdit) AGAINST(:mot)');
        
        
        $mot = mb_strtolower($mot, 'UTF-8');
        $req->bindValue(':mot', $mot, PDO::PARAM_STR);
        $req->execute();
        
        $res = $req->fetch(PDO::FETCH_ASSOC);
        
        if (empty($res)) {
            return false;
        }
        else {
            return true;
        }
    }
}

?>