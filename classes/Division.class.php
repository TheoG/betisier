<?php

class Division {
    
    private $div_num;
    private $div_nom; 
    
    public function __construct($valeurs = array()) {
        if (!empty($valeurs))
            $this->remplir($valeurs);
    }
    
    public function remplir($donnees) {
        
        foreach($donnees as $attribut => $valeur) {
            switch ($attribut) {
                case 'div_num' : 
                    $this->setDiv_num($valeur);
                    break;
                case 'div_nom' :
                    $this->setDiv_nom($valeur);
                    break;
            }
        }
    
    }
    
    public function getDiv_num() {
        return $this->div_num;
    }
    
    public function getDiv_nom() {
        return $this->div_nom;
    }
    
    public function setDiv_num($num) {
        if (is_numeric($num)) {
            if ($num > 0) {
                $this->div_num = $num;
            }
            else {
                echo "Le numéro doit être positif";
            }
        }
        else {
            echo "Le numéro doit être de type entier";
        }
    }

    public function setDiv_nom($nom) {
        if (is_string($nom)) {
            $this->div_nom = $nom;
        }
        else {
            echo "Le nom de la division doit être une chaine de caractère";
        }
    }
}

?>