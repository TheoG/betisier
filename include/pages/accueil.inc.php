<h1>Bienvenue sur le bêtisier de l'IUT !</h1>

<?php
$citationManager = new CitationsManager();
$voteManager = new VoteManager();
$personneManager = new PersonneManager();

$citation = $citationManager->getBestCitation();

if ($citation->getCit_num() != null) {

    $moyenne = $voteManager->getMoyenne($citation->getCit_num());
    $personne = new Personne($personneManager->getPersonne($citation->getPer_num()));

    if (isset($_SESSION['isConnected'])   && 
              $_SESSION['isConnected'] === true && 
              $_SESSION['isAdmin'] === false && 
              $personneManager->isEtudiant($_SESSION['per_num']) !== false)
        $etudiant = true;
    else
        $etudiant = false;

?>

<h3>Meilleur citation du moment : </h3>

<table class="pure-table pure-table-bordered">
    <thead>
        <tr>
            <th>Nom de l'enseignant</th>
            <th>Libellé</th>
            <th>Date</th>
            <th>Moyenne des notes</th>
            <?php if($etudiant === true) { 
                echo '<th>Noter</th>';
            }
            ?>
        </tr>
    </thead>
        <tr>
            <td><?php echo $personne->getPer_prenom(). ' '.$personne->getPer_nom(); ?></td>
            <td><?php echo $citation->getCit_libelle(); ?></td>
            <td><?php echo getFrenchDate($citation->getCit_date()); ?></td>
            <td><?php echo ($moyenne == -1) ? 'Non noté' : number_format($moyenne, 2); ?></td>
            <?php if ($etudiant === true) {
                $vote = new Vote($voteManager->aVote($citation->getCit_num(), $_SESSION['per_num']));
        
                if ($vote->getPer_num() !== null) { ?>
            <td><img src="image/erreur.png" title="Vous avez déjà voté !" alt="aVote"></td>        
                <?php } else { ?>
            <td><a href="<?php echo "index.php?page=".LISTER_CITATION."&amp;id=".$citation->getCit_num() ?>" id="lien"><img src="image/modifier.png" alt="Modifier" title="Voter !"></a></td>
            <?php } 
            } ?>
        </tr>
</table>
<?php } 

$citation = $citationManager->getLastCitation();

if ($citation->getCit_num() != null) {

    $moyenne = $voteManager->getMoyenne($citation->getCit_num());
    $personne = new Personne($personneManager->getPersonne($citation->getPer_num()));

    if (isset($_SESSION['isConnected'])   && 
              $_SESSION['isConnected'] === true && 
              $_SESSION['isAdmin'] === false && 
              $personneManager->isEtudiant($_SESSION['per_num']) !== false)
        $etudiant = true;
    else
        $etudiant = false;
                                      
?>

<h3>Dernière citation ajoutée : </h3>

<table class="pure-table pure-table-bordered">
    <thead>
        <tr>
            <th>Nom de l'enseignant</th>
            <th>Libellé</th>
            <th>Date</th>
            <th>Moyenne des notes</th>
            <?php if($etudiant === true) { 
                echo '<th>Noter</th>';
            }
            ?>
        </tr>
    </thead>
        <tr>
            <td><?php echo $personne->getPer_prenom(). ' '.$personne->getPer_nom(); ?></td>
            <td><?php echo $citation->getCit_libelle(); ?></td>
            <td><?php echo getFrenchDate($citation->getCit_date()); ?></td>
            <td><?php echo ($moyenne == -1) ? 'Non noté' : number_format($moyenne, 2); ?></td>
            <?php if ($etudiant === true) {
                $vote = new Vote($voteManager->aVote($citation->getCit_num(), $_SESSION['per_num']));
        
                if ($vote->getPer_num() !== null) { ?>
            <td><img src="image/erreur.png" alt="aVote"></td>        
                <?php } else { ?>
            <td><a href="<?php echo "index.php?page=".LISTER_CITATION."&amp;id=".$citation->getCit_num() ?>" id="lien"><img src="image/modifier.png" alt="Modifier"></a></td>
            <?php } 
            } ?>
        </tr>
</table>

<?php } ?>


