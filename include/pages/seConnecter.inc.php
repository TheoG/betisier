<script>
    setTitle("Connexion");
</script>

<h1>Se connecter</h1>


<?php
    $reussi = false;
    $trompe = false;

    if (isset($_POST['resultat'])) {
        if ($_SESSION['res'] == $_POST['resultat']) {
            $reussi = true;
        }
        else {
            $trompe = true;
            echo "<p>Mauvais calcul !</p>";
        }
    }
    $nb1 = rand(1, 9);
    $nb2 = rand(1, 9);

    $_SESSION['res'] = $nb1 + $nb2;


if (!$reussi) { ?>
    <form class="pure-form pure-form-aligned" action="#" method="POST">
       <fieldset>
           <div class="pure-control-group">
            <label class="labelConnexion" for="user">Nom d'utilisateur : </label>
            <input id="user" type="text" name="user" value="" required="required">
           </div>
           
           <div class="pure-control-group">
            <label class="labelConnexion" for="passwd">Mot de passe : </label>
            <input id="passwd" type="password" name="passwd" value="" required="required">
           </div>

            <div class="pure-control-group">
                 <img src="image/nb/<?php echo $nb1 ?>.jpg" alt="PremierNombre" />
                 +
                 <img src="image/nb/<?php echo $nb2 ?>.jpg" alt="DeuxiemeNombre" />
                 =
                 <input type="number" name="resultat" value="" required="required">
           </div>
             <input class="pure-button pure-button-primary" type="submit" name="envoyer" value="Valider">
        </fieldset>
    </form>

<?php } 
else { 
    $personneManager = new PersonneManager();

    $personne = new Personne($personneManager->checkLoginAndPassword($_POST['user'], md5(md5($_POST['passwd']).SEL)));
    
    if ($personne->getPer_num() != null) { 
        $_SESSION['isConnected'] = true;
        $_SESSION['login'] = $_POST['user'];
        $_SESSION['per_num'] = $personne->getPer_num();
        
        if ($personne->getPer_admin() == 1)
            $_SESSION['isAdmin'] = true;
        else
            $_SESSION['isAdmin'] = false;
        ?>
        <p><img src="image/valid.png" alt="Valide"> Vous avez bien été connecté</p>
        <p>Redirection automatique dans 2 secondes <img src="image/chargement.gif" alt="Chargement"></p>
    <?php 
         redirige(2);
    }
     else { ?>
       <form class="pure-form pure-form-aligned" action="#" method="POST">
       <fieldset>
           <div class="pure-control-group">
            <label class="labelConnexion" for="user">Nom d'utilisateur : </label>
            <input type="text" name="user" value="" required="required">
           </div>
           
           <div class="pure-control-group">
            <label class="labelConnexion" for="passwd">Mot de passe : </label>
            <input type="password" name="passwd" value="" required="required">
           </div>

            <div class="pure-control-group">
                 <img src="image/nb/<?php echo $nb1 ?>.jpg" alt="PremierNombre" />
                 +
                 <img src="image/nb/<?php echo $nb2 ?>.jpg" alt="DeuxiemeNombre" />
                 =
                 <input type="number" name="resultat" value="" required="required">
           </div>
             <input class="pure-button pure-button-primary" type="submit" name="envoyer" value="Valider">
        </fieldset>
    </form>
        <p>Mauvais nom d'utilisateur ou mot de passe</p>
    <?php }
    } ?>



