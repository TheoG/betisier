<script>
    setTitle("Supprimer une ville");
</script>

<?php
    testAccesDisconnected();
    testAccessAdmin();
?>
   

<?php 
    $villeManager = new VilleManager();
    $departementManager = new DepartementManager();
    $etudiantManager = new EtudiantManager();
    $voteManager = new VoteManager();
    $citationManager = new CitationsManager();
    $personneManager = new PersonneManager();
?>

<h1>Supprimer une ville</h1>

<p>Attention ! Supprimer une ville détruira toutes les citations, étudiants et votes rattachés à celle-ci !</p>
<?php if (!isset($_GET['id'])) { ?>
<table class="pure-table pure-table-bordered sortable">
    <thead>
        <tr>
            <th>Numéro</th>
            <th>Nom</th>
            <th>Supprimer</th>
        </tr>
    </thead>

    <?php

    $listeVilles = $villeManager->getListVille();
    foreach($listeVilles as $ville) { ?>
        <tr>
            <td><?php echo $ville->getVil_num(); ?></td>
            <td><?php echo $ville->getVil_nom(); ?></td>
            <td><a href="<?php echo "index.php?page=".SUPPRIMER_VILLE."&amp;id=".$ville->getVil_num() ?>"><img src="image/erreur.png" alt="Suppression"></a></td>
        </tr>


    <?php } ?>

</table>
<?php } 
else {  
    $vil_num = $_GET['id'];
    //$departement = new Departement($departementManager->getDepartementVille($vil_num));
    $listeDepartement = $departementManager->getDepartementParVil_num($vil_num);
    
    $listeEtudiant = array();
    
    foreach($listeDepartement as $departement) {
        $listeEtudiant = array_merge($listeEtudiant, $etudiantManager->getEtudiantParDepNum($departement->getDep_num()));
    }
    
    // On supprime tous les votes de citations correspondants aux étudiants de la ville donnée
    // On supprime tous les étudiants rattachés à ce département
    foreach($listeEtudiant as $etudiant) {
        $listeVote = $voteManager->getVoteParPerNum($etudiant->getPer_num()); // On récupère tous les votes de chaque étudiant
        $listeCitation = $citationManager->getListCitationParIdEtu($etudiant->getPer_num()); // On récupère toutes les citations de chaque étudiant
        
        foreach($listeVote as $vote) {
            $voteManager->supprimerVote($vote); // On supprime tous les votes
        }
        
        foreach($listeCitation as $citation) {
            $citationManager->supprimerCitation($citation); // On supprime toutes les citations
        }
        $etudiantManager->supprimerEtudiant($etudiant);
        $personneManager->supprimerPersonne($etudiant);
    }
    
    // On supprime les départements rattachés aux villes
    foreach($listeDepartement as $departement) {
        $departementManager->supprimerDepartement($departement);
    }
    
    // On supprime enfin la ville en question :
    $villeManager->supprimerVille($vil_num);
    ?>
    
    <p><img src="image/valid.png" alt="Suppression effectuee"> La ville a bien été supprimée !</p>
    <p>Redirection sur la liste des villes dans 1 seconde <img src="image/chargement.gif" alt="Chargement"></p>
    
    <?php redirigeIndex(1, 'index.php?page='.SUPPRIMER_VILLE);
    } ?>
