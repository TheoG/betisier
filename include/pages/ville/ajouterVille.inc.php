<script>
    setTitle("Ajouter une ville");
</script>

<?php
    testAccesDisconnected();
?>


<?php
$villeManager = new VilleManager();
?>

<h1>Ajouter une ville</h1>

<?php if (!isset($_POST['nom'])) { ?>
<form class="pure-form pure-form-aligned" action="#" method="post">
    <div class="pure-control-group">
        <label for="nom">Nom : </label>
        <input id="nom" type="text" name="nom" value="" required="required">
    </div>
    
    <input class="pure-button pure-button-primary" type="submit" name="envoyer" value="Valider">
</form>

<?php } else {
    $villeManager->addVille($_POST['nom']) ?>
    <p><img src="image/valid.png" alt="Action effectuee" /> La ville <?php echo $_POST['nom'] ?> a été ajoutée !</p>
    <p>Redirection sur la liste des villes dans 2 secondes <img src="image/chargement.gif" alt="Chargement"></p>
        <?php redirigeIndex(2, 'index.php?page='.LISTER_VILLE); ?>
    
<?php } ?>
