<script>
    setTitle("Liste des villes");
</script>

<?php
$villeManager = new VilleManager();
?>

<h1>Liste des villes</h1>

<p><?php echo "Il a actuellement ".$villeManager->getNbVilles()." villes."; ?> </p>

<table class="pure-table pure-table-bordered sortable">
    <thead>
        <tr>
            <th>Numéro</th>
            <th>Nom</th>
        </tr>
    </thead>

    <?php

    $listeVilles = $villeManager->getListVille();
    foreach($listeVilles as $ville) { ?>
    <tr>
        <td><?php echo $ville->getVil_num(); ?></td>
        <td><?php echo $ville->getVil_nom(); ?></td>
    </tr>


    <?php } ?>

</table>
