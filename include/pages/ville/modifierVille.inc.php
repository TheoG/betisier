<script>
    setTitle("Modifier une ville");
</script>

<?php
$villeManager = new VilleManager();
?>

<?php if (!isset($_GET['id']) && !isset($_POST['newName'])) { ?>

<h1>Modifier une ville</h1>

<table class="pure-table pure-table-bordered sortable">
    <thead>
        <tr>
            <th>Numéro</th>
            <th>Nom</th>
            <th>Modifier</th>
        </tr>
    </thead>

    <?php

    $listeVilles = $villeManager->getListVille();
    foreach($listeVilles as $ville) { ?>
    <tr>
        <td><?php echo $ville->getVil_num(); ?></td>
        <td><?php echo $ville->getVil_nom(); ?></td>
        <td><a href="<?php echo "index.php?page=".MODIFIER_VILLE."&amp;id=".$ville->getVil_num() ?>"><img src="image/modifier.png" alt="Modifier"></a></td>
    </tr>
    <?php } ?>
</table>
<?php }
else {
    if(!isset($_POST['newName'])) {
$ville = new Ville($villeManager->getVille($_GET['id']));      
?>
    
<h1>Modification de la ville "<?php echo $ville->getVil_nom(); ?>"</h1>
    
<form action="#" method="post" class="pure-form pure-form-aligned">
   <fieldset>
        <div class="pure-control-group">
            <label for="newName" >Nouveau nom : </label>
            <input id="newName" type="text" name="newName" required="required">
        </div>
       
        <input class="pure-button pure-button-primary" type="submit" value="Modifier">
    </fieldset>
</form>

<?php }
else {
    $villeManager->changerNomVille($_GET['id'], $_POST['newName']); ?>
    
    <p><img src="image/valid.png" alt="Valide"> La ville a bien été renommée !</p>
    <p>Redirection sur la liste des villes dans 1 seconde <img src="image/chargement.gif" alt="Chargement"></p>
    
    <?php redirigeIndex(1, 'index.php?page='.MODIFIER_VILLE);
    }
} ?>