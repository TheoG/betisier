<script>
    setTitle("Se deconnecter");
</script>


<?php
    testAccesDisconnected();
?>
   

<?php   
    unset($_SESSION['login']);
    unset($_SESSION['isConnected']);
    session_destroy();
?>


<h1>Deconnexion</h1>
<p><img src="image/valid.png" alt="Valide"> Vous avez bien été déconnecté</p>
<p>Redirection automatique dans 2 secondes <img src="image/chargement.gif" alt="Chargement"></p>


<?php 
    redirige(2);
?>