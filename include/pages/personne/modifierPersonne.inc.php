<script>
    setTitle("Modifier une personne");
</script>

<?php
    testAccesDisconnected();
    testAccessAdmin();

    if (!isset($_GET['id'])) {
        accueil();
    }
?>

<?php 

$personneManager = new PersonneManager();
$personne = new Personne($personneManager->getPersonne($_GET['id']));
?>

<?php if (!isset($_POST['categorie']) && !isset($_POST['annee']) && !isset($_POST['telPro'])) { ?>
<h1>Modifier une personne enregistrées</h1>
<form class="pure-form pure-form-aligned" action="" method="post">
    <fieldset>
        <div class="pure-control-group">
            <label for="nom">Nom : </label>
            <input type="text" name="nom" value="<?php echo $personne->getPer_nom(); ?>" required="required">
        </div>
        <div class="pure-control-group">
            <label for="prenom">Prénom : </label>
            <input type="text" name="prenom" value="<?php echo $personne->getPer_prenom(); ?>" required="required">
        </div>

        <div class="pure-control-group">
            <label for="tel">Téléphone : </label>
            <input type="text" name="tel" value="<?php echo $personne->getPer_tel(); ?>" required="required">
        </div>

        <div class="pure-control-group">
            <label for="mail">e-mail : </label>
            <input type="email" name="mail" value="<?php echo $personne->getPer_mail(); ?>" required="required">
        </div>

        <div class="pure-control-group">
            <label for="login">Login : </label>
            <input type="text" name="login" value="<?php echo $personne->getPer_login(); ?>" required="required">
        </div>

        <div class="pure-control-group">
            <label for="mdp">Mot de passe : </label>
            <input type="password" name="mdp" value="" required="required">
        </div>
        <div class="pure-control-group">
            <label for="categorie">Catégorie : </label>
            <?php if ($personneManager->isEtudiant($personne->getPer_num())) { ?>
            <input type="radio" name="categorie" value="Personnel" required="required" disabled="disable"> Personnel
            <?php } else { ?>
            <input type="radio" name="categorie" value="Personnel" required="required"> Personnel
            <?php }
            if (!$personneManager->isEtudiant($personne->getPer_num())) { ?>
            <input type="radio" name="categorie" value="Etudiant" required="required" disabled="disable"> Etudiant
            <?php } else { ?>
            <input type="radio" name="categorie" value="Etudiant" required="required"> Etudiant
            <?php } ?>
        </div>

        <input class="pure-button pure-button-primary" type="submit" name="valider" value="Valider">
    </fieldset>
</form>

<?php } else {
        if (!empty($_POST['categorie'])) {            
            if ($personneManager->isEtudiant($personne->getPer_num())) { 
                $divisionManager = new DivisionManager();
                $departementManager = new DepartementManager();
                $_SESSION['nouvellePersonne'] = 
                    array
                    (
                    "per_num" => $_GET['id'],
                    "per_nom" => $_POST['nom'],
                    "per_prenom" => $_POST['prenom'],
                    "per_tel" => $_POST['tel'],
                    "per_mail" => $_POST['mail'],
                    "per_login" => $_POST['login'],
                    "per_pwd" => md5(md5($_POST['mdp']).SEL)
                );
    ?>
    <h1>Modifier un étudiant</h1>

    <form class="pure-form pure-form-aligned" action="" method="post">
        <fieldset>
            <div class="pure-control-group">
                <label for="annee">Année : </label>
                <select name="annee" id="">
                    <?php $listeDiv = $divisionManager->getListDiv();
                foreach($listeDiv as $division) { ?>
                    <option name="annee" value="<?php echo $division->getDiv_num() ?>"><?php echo $division->getDiv_nom() ?></option>
                    <?php } ?>
                </select>
            </div>
            <div class="pure-control-group">
                <label for="dep">Département : </label>
                <select name="dep" id="">
                    <?php $listeDep = $departementManager->getListDep(); 
                foreach($listeDep as $departement) { ?>
                    <option name="dep" value="<?php echo $departement->getDep_num() ?>"><?php echo $departement->getDep_nom() ?></option>
                    <?php } ?>
                </select>
            </div>
            <input class="pure-button pure-button-primary" type="submit" name="valider" value="Valider">
        </fieldset>
    </form>

    <?php } else { 
                $fonctionManager = new FonctionManager();

                $_SESSION['nouvellePersonne'] = 
                    array
                    (
                    "per_num" => $_GET['id'],
                    "per_nom" => $_POST['nom'],
                    "per_prenom" => $_POST['prenom'],
                    "per_tel" => $_POST['tel'],
                    "per_mail" => $_POST['mail'],
                    "per_login" => $_POST['login'],
                    "per_pwd" => md5(md5($_POST['mdp']).SEL)
                );


    ?>
    <h1>Modifier un salarié</h1>

    <form class="pure-form pure-form-aligned" action="" method="post">
        <div class="pure-control-group">
            <label for="telPro">Téléphone professionnel : </label>
            <input type="text" name="telPro" required="required" pattern="^(?:0|\(?\+33\)?\s?|0033\s?)[1-79](?:[\.\-\s]?\d\d){4}$">
        </div>
        <div class="pure-control-group">
            <label for="fonction">Fonction : </label>
            <select name="fonction" id="">
                <?php $listeFonction = $fonctionManager->getListFonction(); 
                foreach($listeFonction as $fonction) { ?>
                <option name="fonction" value="<?php echo $fonction->getFon_num(); ?>"><?php echo $fonction->getFon_libelle(); ?></option>
                <?php } ?>
            </select>
        </div>
        <input class="pure-button pure-button-primary" type="submit" name="valider" value="Valider">

    </form>

    <?php }
        } else {
            $personneManager = new PersonneManager();
            if (isset($_POST['annee'])) {
                $etudiantManager = new EtudiantManager();
                $etudiant;


                $_SESSION['nouvellePersonne']['per_admin'] = 0;
                $personneManager->modifierPersonne(new Personne($_SESSION['nouvellePersonne']));

                $etudiant = new Personne($personneManager->getPersonnePerLogin($_SESSION['nouvellePersonne']['per_login']));

                $_SESSION['nouvellePersonne']['dep_num'] = $_POST['dep'];
                $_SESSION['nouvellePersonne']['div_num'] = $_POST['annee'];
                $_SESSION['nouvellePersonne']['per_num'] = $etudiant->getPer_num();

                $etudiantManager->modifierEtudiant(new Etudiant($_SESSION['nouvellePersonne']));
                unset($_SESSION['nouvellePersonne']);
    ?> 
    <p><img src="image/valid.png" alt="Valide"> L'étudiant a été modifié !</p>
    <p>Redirection sur la liste des personnes dans 1 seconde <img src="image/chargement.gif" alt="Chargement"></p>

        <?php redirigeIndex(1, 'index.php?page='.LISTER_PERSONNE);
    }
            else if (isset($_POST['telPro'])) {
                $salarieManager = new SalarieManager();
                $salarie;

                $_SESSION['nouvellePersonne']['per_admin'] = 1;
                $personneManager->modifierPersonne(new Personne($_SESSION['nouvellePersonne']));

                $salarie = new Personne($personneManager->getPersonnePerLogin($_SESSION['nouvellePersonne']['per_login']));

                $_SESSION['nouvellePersonne']['per_num'] = $salarie->getPer_num();
                $_SESSION['nouvellePersonne']['sal_telprof'] = $_POST['telPro'];
                $_SESSION['nouvellePersonne']['fon_num'] = $_POST['fonction'];

                $salarieManager->modifierSalarie(new Salarie($_SESSION['nouvellePersonne']));
                unset($_SESSION['nouvellePersonne']);
    ?>

    <p><img src="image/valid.png" alt="Valide"> Le salarié a été modifié !</p>
        <p>Redirection sur la liste des personnes dans 1 seconde <img src="image/chargement.gif" alt="Chargement"></p>

        <?php redirigeIndex(1, 'index.php?page='.LISTER_PERSONNE);
            }
    }
}
?>
