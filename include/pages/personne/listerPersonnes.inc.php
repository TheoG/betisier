<script>
    setTitle("Lister des personnes");
</script>
   

<?php
    $personneManager = new PersonneManager();
?>


<?php
if (!isset($_GET['id'])) { ?>
<h1>Liste des personnes enregistrées</h1>


<p><?php echo "Il a actuellement ".$personneManager->getNbPersonnes()." personnes enregistrées."; ?> </p>
<p>Cliquez sur le numéro de la personne pour obtenir plus d'informations</p>


<table class="pure-table pure-table-bordered sortable">
   <thead>
    <tr>
        <th>Numéro</th>
        <th>Nom</th>
        <th>Prénom</th>
        <?php if(isset($_SESSION['isConnected']) && $_SESSION['isAdmin'] === true) { 
            echo '<th>Modifier</th>';
        } ?>
    </tr>
    </thead>

    <?php
    $listePersonnes = $personneManager->getListPersonnes();
    foreach($listePersonnes as $personne) { ?>
        <tr>
            <td><a href="<?php echo "index.php?page=".LISTER_PERSONNE."&amp;id=".$personne->getPer_num() ?>" class="lien"><?php echo $personne->getPer_num(); ?></a></td>
            <td><?php echo $personne->getPer_nom(); ?></td>
            <td><?php echo $personne->getPer_prenom(); ?></td>
            <?php if(isset($_SESSION['isConnected']) && $_SESSION['isAdmin'] === true) { ?>
            <td><a href=<?php echo "index.php?page=".MODIFIER_PERSONNE."&amp;id=".$personne->getPer_num(); ?> ><img src="image/modifier.png" alt="Modifier"></a></td>
                <?php
            } ?>
        </tr>

    <?php } ?>

</table>


<?php } else {
    $id = $_GET['id'];

    $personne = new Personne($personneManager->getPersonne($id));
    $isPersonnel = $personneManager->isEtudiant($id);


    if ($isPersonnel === false) {
        $salarieManager = new SalarieManager();
        $fonctionManager = new FonctionManager();

        $salarie = new Salarie($salarieManager->getSalarie($id));
        $fonction = new Fonction($fonctionManager->getFonction($salarie->getFon_num())); ?>


        <h1>Détail sur le salarié <?php echo $personne->getPer_nom() ?> </h1>

        <table class="pure-table pure-table-bordered">
            <thead>
                <tr>
                    <th>Prénom</th>
                    <th>Mail</th>
                    <th>Tel</th>
                    <th>Tel Pro</th>
                    <th>Fonction</th>
                </tr>
            </thead>

            <tr>
                <td><?php echo $salarie->getPer_prenom(); ?></td>
                <td><?php echo $salarie->getPer_mail(); ?></td>
                <td><?php echo $salarie->getPer_tel() ?></td>
                <td><?php echo $salarie->getSal_telProf() ?></td>
                <td><?php echo $fonction->getFon_libelle() ?></td>
            </tr>
        </table>

    <?php }
    else {
        $etudiantManager = new EtudiantManager();
        $departementManager = new DepartementManager();
        $villeManager = new VilleManager();

        $etudiant = new Etudiant($etudiantManager->getEtudiant($id));
        $departement = new Departement($departementManager->getDepartement($etudiant->getDep_num()));
        $ville = new Ville($villeManager->getVille($departement->getVil_num())); ?>

  <h1>Détail sur l'étudiant <?php echo $personne->getPer_nom() ?></h1>

        <table class="pure-table pure-table-bordered">
            <thead>
               <tr>
                    <th>Prénom</th>
                    <th>Mail</th>
                    <th>Tel</th>
                    <th>Département</th>
                    <th>Ville</th>
                </tr>
            </thead>
            <tr>
                <td><?php echo $etudiant->getPer_prenom() ?></td>
                <td><?php echo $etudiant->getPer_mail(); ?></td>
                <td><?php echo $etudiant->getPer_tel() ?></td>
                <td><?php echo $departement->getDep_nom() ?> </td>
                <td><?php echo $ville->getVil_nom() ?></td>
            </tr>
        </table>

    <?php }

?>


<?php } ?>
