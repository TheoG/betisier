<script>
    setTitle("Supprimer une personne");
</script>

<?php
    testAccesDisconnected();
    testAccessAdmin();
?>

<h1>Supprimer des personnes enregistrées</h1>

<?php

    $personneManager = new PersonneManager();


if (!isset($_GET['id'])) { ?>
<table class="pure-table pure-table-bordered sortable">
   <thead>
    <tr>
        <th>Numéro</th>
        <th>Nom</th>
        <th>Prénom</th>
        <?php if(isset($_SESSION['isConnected']) && $_SESSION['isAdmin'] === true) { 
            echo '<th>Supprimer</th>';
        } ?>
    </tr>
    </thead>

    <?php
    $listePersonnes = $personneManager->getListPersonnes();
    foreach($listePersonnes as $personne) { ?>
        <tr>
            <td><a href="<?php echo "index.php?page=".LISTER_PERSONNE."&amp;id=".$personne->getPer_num() ?>" class="lien"><?php echo $personne->getPer_num(); ?></a></td>
            <td><?php echo $personne->getPer_nom(); ?></td>
            <td><?php echo $personne->getPer_prenom(); ?></td>
            <?php if(isset($_SESSION['isConnected']) && $_SESSION['isAdmin'] === true) { ?>
            <td><a href="<?php echo "index.php?page=".SUPPRIMER_PERSONNE."&amp;id=".$personne->getPer_num(); ?>" ><img src="image/erreur.png" alt="Supprimer"></a></td>
                <?php
            } ?>
        </tr>

    <?php } ?>

</table>

<?php }
else {
    $personne = new Personne($personneManager->getPersonne($_GET['id']));
?>
   <?php 
    if ($personneManager->isEtudiant($personne->getPer_num())) {
        $etudiantManager = new EtudiantManager();
        $voteManager = new VoteManager();
        $citationManager = new CitationsManager();
        
        // On supprime d'abord tous les votes qui se rapportent aux citations déposées par l'étudiant qui va être supprimé
        // ou bien aux votes que l'etudiant aurait pu éventuellement accepté s'il était admin dans une vie antérieure
        // Puis, dans la même boucle, on supprime les citations déposées
        $listeCitation = $citationManager->getListCitationParIdEtu($personne->getPer_num());
        $listeCitation = array_merge($listeCitation, $citationManager->getListCitationParIdEtuValide($personne->getPer_num()));

        $voteManager->supprimerVoteParPersonne($personne);
        foreach($listeCitation as $citation) {
            $voteManager->supprimerVoteParCitation($citation->getCit_num());
            $citationManager->supprimerCitationParNum($citation->getCit_num());
        }
        
        // On supprime maintenant l'étudiant
        $etudiantManager->supprimerEtudiant($personne);
        
        // On supprime enfin la personne
        $personneManager->supprimerPersonne($personne);
    }
    else {
        $salarieManager = new SalarieManager();
        $citationManager = new CitationsManager();
        $voteManager = new VoteManager();
        
        // On supprime d'abord tous les votes qui se rapportent aux citations provenant du membre de personnel qui va être supprimé
        // Puis, dans la même boucle, on supprime les citations concernées
        $listeCitation = $citationManager->getListCitationParIdPerso($personne->getPer_num());
        
        foreach($listeCitation as $citation) {
            $voteManager->supprimerVoteParCitation($citation->getCit_num());
            $citationManager->supprimerCitationParNum($citation->getCit_num());
        }
        
        // On supprime maintenant le salarie
        $salarieManager->supprimerSalarie($personne);
        
        // On supprime enfin la personne
        $personneManager->supprimerPersonne($personne);
    }
    
    ?>
    <p><img src="image/valid.png" alt="Valide"> La personne a bien été supprimée !</p>
    <p>Redirection sur la liste des personnes dans 1 seconde <img src="image/chargement.gif" alt="Chargement"></p>
    
    <?php redirigeIndex(1, 'index.php?page='.SUPPRIMER_PERSONNE);
 } ?>