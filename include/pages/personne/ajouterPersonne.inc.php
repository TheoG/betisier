<script>
    setTitle("Ajouter une personne");
</script>
   

<?php
    testAccesDisconnected();
    $personneManager = new PersonneManager();
?>
<?php if (!isset($_POST['categorie']) && !isset($_POST['annee']) && !isset($_POST['telPro'])) {
    include('include/formulaires/ajouterPersonneForm.inc.php');
}

else {
    if (isset($_POST['login']) && $personneManager->loginExist($_POST['login']) == true) { 
        redirigeIndex(1, 'index.php?page='.AJOUTER_PERSONNE); ?>
        <p><img src="image/erreur.png" title="Erreur" alt="Erreur">Le login existe déjà !</p>
        <p>Redirection sur le formulaire dans 1 seconde</p>
    <?php } else {
    
        if (!empty($_POST['categorie'])) {
            if ($_POST['categorie'] == 'Etudiant') { 
                $divisionManager = new DivisionManager();
                $departementManager = new DepartementManager();
                $_SESSION['nouvellePersonne'] = 
                    array
                    (
                    "per_nom" => $_POST['nom'],
                    "per_prenom" => $_POST['prenom'],
                    "per_tel" => $_POST['tel'],
                    "per_mail" => $_POST['mail'],
                    "per_login" => $_POST['login'],
                    "per_pwd" => md5(md5($_POST['mdp']).SEL)
                );

                include('include/formulaires/ajouterEtudiantForm.inc.php');

            } else { 
                $fonctionManager = new FonctionManager();

                $_SESSION['nouvellePersonne'] = 
                    array
                    (
                    "per_nom" => $_POST['nom'],
                    "per_prenom" => $_POST['prenom'],
                    "per_tel" => $_POST['tel'],
                    "per_mail" => $_POST['mail'],
                    "per_login" => $_POST['login'],
                    "per_pwd" => md5(md5($_POST['mdp']).SEL)
                );

                include('include/formulaires/ajouterSalarieForm.inc.php');
            }
        } else {
            if (isset($_POST['annee'])) {
                $etudiantManager = new EtudiantManager();
                $etudiant;


                $_SESSION['nouvellePersonne']['per_admin'] = 0;
                $personneManager->addPersonne(new Personne($_SESSION['nouvellePersonne']));

                $etudiant = new Personne($personneManager->getPersonnePerLogin($_SESSION['nouvellePersonne']['per_login']));

                $_SESSION['nouvellePersonne']['dep_num'] = $_POST['dep'];
                $_SESSION['nouvellePersonne']['div_num'] = $_POST['annee'];
                $_SESSION['nouvellePersonne']['per_num'] = $etudiant->getPer_num();

                $etudiantManager->addEtudiant(new Etudiant($_SESSION['nouvellePersonne']));
                unset($_SESSION['nouvellePersonne']);


    ?> 
    <p><img src="image/valid.png" alt="Valide"> L'étudiant a été ajouté !</p>
    <p>Redirection sur la liste des personnes dans 1 seconde <img src="image/chargement.gif" alt="Chargement"></p>

        <?php redirigeIndex(1, 'index.php?page='.LISTER_PERSONNE);
        }
            else if (isset($_POST['telPro'])) {
                $salarieManager = new SalarieManager();
                $salarie;

                $_SESSION['nouvellePersonne']['per_admin'] = 0;
                $personneManager->addPersonne(new Personne($_SESSION['nouvellePersonne']));

                $salarie = new Personne($personneManager->getPersonnePerLogin($_SESSION['nouvellePersonne']['per_login']));

                $_SESSION['nouvellePersonne']['per_num'] = $salarie->getPer_num();
                $_SESSION['nouvellePersonne']['sal_telprof'] = $_POST['telPro'];
                $_SESSION['nouvellePersonne']['fon_num'] = $_POST['fonction'];

                $salarieManager->addSalarie(new Salarie($_SESSION['nouvellePersonne']));
                unset($_SESSION['nouvellePersonne']);
    ?>

    <p><img src="image/valid.png" alt="Valide"> Le salarié a été ajouté !</p>

    <p>Redirection sur la liste des personnes dans 1 seconde <img src="image/chargement.gif" alt="Chargement"></p>

        <?php redirigeIndex(1, 'index.php?page='.LISTER_PERSONNE);
            }
        }
    }
}
?>
