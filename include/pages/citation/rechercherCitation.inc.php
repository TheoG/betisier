<script>
    setTitle("Rechercher une citation");
</script>

<?php
    testAccesDisconnected();
    $personneManager = new PersonneManager();

    if (isset($_SESSION['isConnected'])   && 
              $_SESSION['isConnected'] === true && 
              $_SESSION['isAdmin'] === false && 
              $personneManager->isEtudiant($_SESSION['per_num']) !== false)
        $etudiant = true;
    else
        $etudiant = false;
?>

<h1>Rechercher une citation :</h1>

<?php if(empty($_POST['recherche'])) { ?>
<form class="pure-form pure-form-aligned" action="#" method="post">
   <div class="pure-control-group">
        <label for="choix">Recherche par : </label>
        <input type="checkbox" name="enseignant" checked="checked"> Enseignant
        <input type="checkbox" name="date"> Date
        <input type="checkbox" name="note"> Note
        <input type="checkbox" name="libelle"> Libelle
    </div>
    
    <div class="pure-control-group">
        <label for="recherche">Mots clefs : </label>
        <input id="recherche" type="text" name="recherche" required="required">
    </div>
    <input class="pure-button pure-button-primary" type="submit" value="Rechercher">
    
</form>
<?php } else {
    $citationManager = new CitationsManager();
    $personneManager = new PersonneManager();
    $voteManager = new VoteManager();
    
    isset($_POST['enseignant']) ? $enseignant   = $_POST['enseignant']  : $enseignant = "";
    isset($_POST['date'])       ? $date         = $_POST['date']        : $date = "";
    isset($_POST['note'])       ? $note         = $_POST['note']        : $note = "";
    isset($_POST['libelle'])    ? $libelle      = $_POST['libelle']     : $libelle = "";
    
    $listeCitations = $citationManager->rechercher($enseignant, $date, $note, $libelle, $_POST['recherche']);

    if (!empty($listeCitations)) { ?>
<table class="pure-table pure-table-aligned">
    <thead>
        <tr>
            <th>Nom de l'enseignant</th>
            <th>Libellé</th>
            <th>Date</th>
            <th>Moyenne des notes</th>
            <?php if($etudiant === true) { 
                echo '<th>Noter</th>';
            }
            ?>
        </tr>
    </thead>
    <tbody>
        <?php
        foreach($listeCitations as $citation) { 
            $personne = new Personne($personneManager->getPersonne($citation->getPer_num()));

            $moyenne = $voteManager->getMoyenne($citation->getCit_num()); ?>

            <tr>
                <td><?php echo $personne->getPer_prenom(). ' '.$personne->getPer_nom(); ?></td>
                <td><?php echo $citation->getCit_libelle(); ?></td>
                <td><?php echo getFrenchDate($citation->getCit_date()); ?></td>
                <td><?php echo ($moyenne == -1) ? 'Non noté' : number_format($moyenne, 2); ?></td>
                <?php if ($etudiant === true) {
                $vote = new Vote($voteManager->aVote($citation->getCit_num(), $_SESSION['per_num']));
                    if ($vote->getPer_num() !== null) { ?>
                        <td><img src="image/erreur.png" title="Vous avez déjà voté !" alt="aVote"></a></td>        
                <?php } else { ?>
                        <td><a href="<?php echo "index.php?page=".LISTER_CITATION."&id=".$citation->getCit_num() ?>" id="lien"><img src="image/modifier.png" title="Votez cette citation !" alt="Modifier"></a></td>
                <?php } 
                } ?>
            </tr>
        <?php } ?>
    </tbody>
</table>
<form action="index.php?page=<?php echo RECHERCHER_CITATION ?>" method="post">
        <input class="pure-button " type="submit" value="Recommencer">
</form>
<?php }
else { ?>
    <p>Aucun résultat !</p>
    <form action="index.php?page=<?php echo RECHERCHER_CITATION ?>" method="post">
        <input class="pure-button " type="submit" value="Recommencer">
    </form>
     <?php }
    } ?>