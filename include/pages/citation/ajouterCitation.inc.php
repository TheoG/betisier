<script>
    setTitle("Ajouter une citation");
</script>
   

<?php
    testAccesDisconnected();
?>


<script>
    $(function() {
        $( ".datepicker" ).datepicker({maxDate: '0'});
    });
</script>
<h1>Ajouter une citation</h1>


<?php if(empty($_POST['citation'])) {
    
    include('include/formulaires/ajouterCitationForm.inc.php');

} else {
        // On vérifie s'il y a des mots interdits, et on les affiche par la suite
    
        $motManager = new MotManager();
        $mots = explode(" ", $_POST['citation']);

        $erreur = false;
        foreach($mots as $liste => $mot) {
            if ($motManager->isMotInterdit($mot) === true) {
                $erreur = true;
            }
        }

        if ($erreur === false) {
            $citationManager = new CitationsManager();
            
            $citation = array(
                "per_num" => $_POST['enseignant'],
                "per_num_valide" => NULL,
                "per_num_etu" => $_SESSION['per_num'],
                "cit_libelle" => $_POST['citation'],
                "cit_date" => getEnglishDate($_POST['dateCitation']),
                "cit_valide" => 0,
                "cit_date_valide" => NULL,
                "cit_date_depo" => date('Y-m-d H:i:s')
            );
            $citationManager->addCitation(new Citations($citation)); ?>
            
            <p><img src="image/valid.png" alt="Valide"> La citation a bien été ajoutée</p>
            <p>Redirection sur la liste des citations dans 1 seconde <img src="image/chargement.gif" alt="Chargement"></p>
        <?php redirigeIndex(1, 'index.php?page='.LISTER_CITATION);
        }
        else { ?>

<form class="pure-form pure-form-aligned" action="" method="post">
    <fieldset>
        <p class="pure-control-group">
            <label for="enseignant">Enseignant : </label>
            <select name="enseignant" id="">
                <?php
                    $salarieManager = new SalarieManager();

                    foreach($salarieManager->getListSalarie() as $salarie) { 
                        $num = $salarie->getPer_num();
                        $nom = $salarie->getPer_prenom(). ' '.$salarie->getPer_nom();
                        
                        echo '<option ';
                        if ($_POST['enseignant'] === $num) {
                            echo 'selected ';
                        }
                        echo 'value='.$num.'>';
                        echo $nom.'</option>'."\n";
                    } ?>
            </select>
        </p>
        <p class="pure-control-group">
            <label for="dateCitation">Date de la citation : </label>
            <input type="text" id="dateCitation" class="datepicker" name="dateCitation" required="required" value="<?php echo $_POST['dateCitation']; ?>">
        </p>
        <p class="pure-control-group">
            <label for="citation">Citation : </label>
                <textarea required="required" name="citation" id="" cols="30" rows="3"><?php
                        $motManager = new MotManager();
                        $mots = explode(' ', $_POST['citation']);
                        foreach($mots as $liste => $mot) {
                            if ($motManager->isMotInterdit($mot) === true) {
                                
                                $motsErreur[] = $mot;
                                $erreur = true;
                                echo '---';
                            }
                            else {
                                $erreur = false;
                                echo $mot;
                            }
                            echo ' ';
                        }
                    ?></textarea>
            <?php foreach($motsErreur as $mot) { ?>
                        <p><img src="image/erreur.png" alt="Erreur"> Le mot <?php echo $mot; ?> n'est pas autorisé</p>
                    <?php } ?>
        </p>
        <input class="pure-button pure-button-primary" type="submit" value="Valider">
    </fieldset>
</form>

        <?php }
} ?>
