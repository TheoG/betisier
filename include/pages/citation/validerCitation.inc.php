<script>
    setTitle("Valider une citation");
</script>

<?php
    testAccesDisconnected();
    testAccessAdmin();
?>

<h1>Valider une citation</h1>

<?php

$citationManager = new CitationsManager();
$personneManager = new PersonneManager();

if (isset($_GET['id'])) { ?>
    <p><img src="image/valid.png" alt="Valide">  Citation validée !</p>
    <p>Redirection sur la liste des citations à valider dans 1 seconde <img src="image/chargement.gif" alt="Chargement"></p>
    
    <?php
        $citationManager->valider($_GET['id'], $_SESSION['per_num']);
        redirigeIndex(1, 'index.php?page='.VALIDER_CITATION);
}

if (!isset($_GET['id'])) {
    $listeCitations = $citationManager->getListCitationInvalide();
    
    if (empty($listeCitations)) { ?>
         <p>Il n'y a aucune citation a valider !</p>
    <?php } else { ?>
    
<table class="pure-table pure-table-bordered sortable">
    <thead>
        <tr>
            <th>Nom de l'enseignant</th>
            <th>Libellé</th>
            <th>Date</th>
            <th>Valider</th>
        </tr>
    </thead>
    
<?php
    foreach($listeCitations as $citation) { 
        $personne = new Personne($personneManager->getPersonne($citation->getPer_num())); ?>
        <tr>
            <td><?php echo $personne->getPer_prenom(). ' '.$personne->getPer_nom(); ?></td>
            <td><?php echo $citation->getCit_libelle(); ?></td>
            <td><?php echo getFrenchDate($citation->getCit_date()); ?></td>
            <td><a href="<?php echo "index.php?page=".VALIDER_CITATION."&amp;id=".$citation->getCit_num() ?>"><img src="image/valid.png" alt="Valider"></a></td>
        </tr>    
        
    <?php } ?>
</table>
<?php } 
} ?>