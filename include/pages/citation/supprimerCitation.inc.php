<script>
    setTitle("Supprimer une citation");
</script>

<?php
    testAccesDisconnected();
    testAccessAdmin();
?>

<h1>Supprimer une citation</h1>

<?php
$citationManager = new CitationsManager();
$voteManager = new VoteManager();
$personneManager = new PersonneManager();

if (isset($_SESSION['isConnected'])   && 
    $_SESSION['isConnected'] === true && 
    $_SESSION['isAdmin'] === false && 
    $personneManager->isEtudiant($_SESSION['per_num']) !== false)
    $etudiant = true;
else
    $etudiant = false;


if (!isset($_GET['id'])) { ?>
<table class="pure-table pure-table-bordered sortable">
   <thead>
        <tr>
            <th>Nom de l'enseignant</th>
            <th>Libellé</th>
            <th>Date</th>
            <th>Moyenne des notes</th>
            <th>Supprimer</th>
        </tr>
    </thead>

    <?php

    $listeCitations = $citationManager->getListCitation();
    foreach($listeCitations as $citation) { 
        $personne = new Personne($personneManager->getPersonne($citation->getPer_num()));

        $moyenne = $voteManager->getMoyenne($citation->getCit_num());

    ?>
    <tr>
        <td><?php echo $personne->getPer_prenom(). ' '.$personne->getPer_nom(); ?></td>
        <td><?php echo $citation->getCit_libelle(); ?></td>
        <td><?php echo getFrenchDate($citation->getCit_date()); ?></td>
        <td><?php echo ($moyenne == -1) ? 'Non noté' : number_format($moyenne, 2); ?></td>
        <td><a href="<?php echo "index.php?page=".SUPPRIMER_CITATION."&amp;id=".$citation->getCit_num() ?>"><img src="image/erreur.png" alt="Suppression"></a></td>
    </tr>


    <?php } ?>

</table>


<?php }
else { 
    $voteManager->supprimerVoteParCitation($_GET['id']);
    $citationManager->supprimerCitationParNum($_GET['id']);
?>
    <p><img src="image/valid.png" alt="Suppression effectuee"> La citation a bien été supprimée !</p>
    <p>Redirection sur la liste des citations dans 1 seconde <img src="image/chargement.gif" alt="Chargement"></p>
    
    <?php redirigeIndex(1, 'index.php?page='.SUPPRIMER_CITATION);

} ?>