<script>
    setTitle("Liste des citations");
</script>

<?php
    $citationManager = new CitationsManager();
    $voteManager = new VoteManager();
    $personneManager = new PersonneManager();

    if (isset($_SESSION['isConnected'])   && 
              $_SESSION['isConnected'] === true && 
              $_SESSION['isAdmin'] === false && 
              $personneManager->isEtudiant($_SESSION['per_num']) !== false)
        $etudiant = true;
    else
        $etudiant = false;
?>

<?php if(empty($_GET['id'])) { ?>
<h1>Liste des citations déposées</h1>

<p><?php echo "Il y a actuellement ".$citationManager->getNbCitations()." citations enregistrées."; ?> </p>
<?php if ($citationManager->getNbCitations() != 0) { 
    if (isset($_SESSION['isConnected'])) {
        $listeCitations = $citationManager->getListCitation();
    }
    else {
        $listeCitations = $citationManager->getListCitationNonConnecte(); ?>
        <p>Seule les 2 premières citations seront affichées ici. Connectez-vous pour voir les autres !</p>
    <?php }
?>
<table class="pure-table pure-table-bordered sortable">
    <thead>
        <tr>
            <th>Nom de l'enseignant</th>
            <th>Libellé</th>
            <th>Date</th>
            <th>Moyenne des notes</th>
            <?php if($etudiant === true) { 
                echo '<th>Noter</th>';
            }
            ?>
        </tr>
    </thead>
    
    <?php

    foreach($listeCitations as $citation) { 
        $personne = new Personne($personneManager->getPersonne($citation->getPer_num()));
        
        $moyenne = $voteManager->getMoyenne($citation->getCit_num());
    
    ?>
        <tr>
            <td><?php echo $personne->getPer_prenom(). ' '.$personne->getPer_nom(); ?></td>
            <td><?php echo $citation->getCit_libelle(); ?></td>
            <td><?php echo getFrenchDate($citation->getCit_date()); ?></td>
            <td><?php echo ($moyenne == -1) ? 'Non noté' : number_format($moyenne, 2); ?></td>
            <?php if ($etudiant === true) {
                $vote = new Vote($voteManager->aVote($citation->getCit_num(), $_SESSION['per_num']));
        
                if ($vote->getPer_num() !== null) { ?>
            <td><img src="image/erreur.png" title="Vous avez déjà voté !" alt="aVote"></a></td>        
                <?php } else { ?>
            <td><a href="<?php echo "index.php?page=".LISTER_CITATION."&id=".$citation->getCit_num() ?>" id="lien"><img src="image/modifier.png" title="Votez cette citation !" alt="Modifier"></a></td>
            <?php } 
            } ?>
        </tr>
        
        
    <?php } ?>
    
</table>
<?php }
} else if ($etudiant === true) {
    if (!isset($_POST['note'])) { ?>
<h1>Notez une citation : </h1>

<?php 
    if ($etudiant === false) {
        redirigeIndex(0, 'index.php?page='.LISTER_CITATION);
    }
    $vote = new Vote($voteManager->aVote($_GET['id'], $_SESSION['per_num']));
    if ($vote->getPer_num() !== null) { ?>
        <p>Vous avez déjà voté.</p>
    <?php 
        redirigeIndex(2, 'index.php?page='.LISTER_CITATION);
        } else {
                                
?>

<form class="pure-form pure-form-aligned" action="" method="post">
  <fieldset>
        <div class="pure-control-group">
            <label for="note">Note : </label>
            <input name="note" type="number" required="required" min="0" max="20">
        </div>

        <input class="pure-button pure-button-primary" type="submit">
    </fieldset>
</form>
<?php } } else { 
    $voteManager->voter($_SESSION['per_num'], $_GET['id'], $_POST['note']); ?>
    <p><img src="image/valid.png" alt="Valide">  Votre note (<?php echo $_POST['note']; ?>) a bien été prise en compte.</p>
    <p>Redirection sur la liste des citations dans 1 secondes <img src="image/chargement.gif" alt="Chargement"></p>
    <?php 
    redirigeIndex(1, 'index.php?page='.LISTER_CITATION);
    } 
}
else {
    echo '<p>Vous devez être un étudiant pour noter.</p>';
}
?>