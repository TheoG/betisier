<div id="texte">
    <?php
    if (!empty($_GET["page"])){
        $page=$_GET["page"];}
    else
    {
        $page=0;
    }
    switch ($page) {
        case 0:
            // Page d'accueik
            include_once('pages/accueil.inc.php');
            break;
        case 1:
            // Ajouter une nouvelle personne
            include("pages/personne/ajouterPersonne.inc.php");
            break;

        case 2:
            // Lister les personnes
            include_once('pages/personne/listerPersonnes.inc.php');
            break;
        case 3:
            // Modifier les personnes
            include("pages/personne/modifierPersonne.inc.php");
            break;
        case 4:
            // Supprimer les personnes
            include_once('pages/personne/supprimerPersonne.inc.php');
            break;
        case 5:
            // Ajouter une citation
            include("pages/citation/ajouterCitation.inc.php");
            break;

        case 6:
            // Lister les citations
            include("pages/citation/listerCitation.inc.php");
            break;
            //
            // Villes
            //

        case 7:
            // Ajouter une ville
            include("pages/ville/ajouterVille.inc.php");
            break;

        case 8:
            // Lister les villes
            include("pages/ville/listerVilles.inc.php");
            break;

            //

            //
        case 9:
            // Page de connexion
            include ("pages/seConnecter.inc.php");
            break;
        case 10:
            // Page de deconnexion
            include ("pages/deconnexion.inc.php");
            break;
        case 11:
            // Modifier une ville
            include ("pages/ville/modifierVille.inc.php");
            break;
        case 12:
            // Supprimer une ville
            include("pages/ville/supprimerVille.inc.php");
            break;
        case 13:
            // Rechercher une citation
            include("pages/citation/rechercherCitation.inc.php");
            break;
        case 14:
            // Valider une citation
            include("pages/citation/validerCitation.inc.php");
            break;
        case 15:
            // Supprimer une citation
            include("pages/citation/supprimerCitation.inc.php");
            break;
            // Page 404
        default : 	include_once('pages/erreur404.inc.php');
    }

    ?>
</div>
