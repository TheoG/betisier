<h1>Ajouter un étudiant</h1>

<form class="pure-form pure-form-aligned" action="#" method="post">
   <fieldset>
   <div class="pure-control-group">
        <label for="annee">Année : </label>
        <select name="annee" id="">
            <?php $listeDiv = $divisionManager->getListDiv();
                foreach($listeDiv as $division) { ?>
            <option name="annee" value="<?php echo $division->getDiv_num() ?>"><?php echo $division->getDiv_nom() ?></option>
            <?php } ?>
        </select>
    </div>
    <div class="pure-control-group">
        <label for="dep">Département : </label>
        <select name="dep" id="">
            <?php $listeDep = $departementManager->getListDep(); 
                foreach($listeDep as $departement) { ?>
            <option name="dep" value="<?php echo $departement->getDep_num() ?>"><?php echo $departement->getDep_nom() ?></option>
            <?php } ?>
        </select>
    </div>
    <input class="pure-button pure-button-primary" type="submit" name="valider" value="Valider">
    </fieldset>
</form>