<form class="pure-form pure-form-aligned" action="#" method="post">
    <fieldset>
        <div class="pure-control-group">
            <label for="enseignant">Enseignant : </label>
            <select name="enseignant" id="enseignant">
                <?php
                    $salarieManager = new SalarieManager();

                    foreach($salarieManager->getListSalarie() as $salarie) { 
                        $num = $salarie->getPer_num();
                        $nom = $salarie->getPer_prenom(). ' '.$salarie->getPer_nom(); ?>
                        <option value="<?php echo $num; ?>"><?php echo $nom; ?></option>
                    <?php } ?>
            </select>
        </div>
        <div class="pure-control-group">
            <label for="dateCitation">Date de la citation : </label>
            <input type="text" class="datepicker" id="dateCitation" name="dateCitation" required="required" pattern="(0[1-9]|1[0-9]|2[0-9]|3[01]).(0[1-9]|1[012]).[0-9]{4}">
        </div>
        <div class="pure-control-group">
            <label for="citation">Citation : </label>
                <textarea required="required" name="citation" id="citation" cols="30" rows="3"></textarea>
        </div>
            <input class="pure-button pure-button-primary" type="submit" value="Valider">
    </fieldset>
</form>