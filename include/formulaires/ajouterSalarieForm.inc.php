<h1>Ajouter un salarié</h1>

<form class="pure-form pure-form-aligned" action="#" method="post">
   <fieldset>
       <div class="pure-control-group">
            <label for="telPro">Téléphone professionnel : </label>
            <input type="text" id="telPro" name="telPro" value="" required="required" pattern="^(?:0|\(?\+33\)?\s?|0033\s?)[1-79](?:[\.\-\s]?\d\d){4}$">
       </div>
       <div class="pure-control-group">
            <label for="fonction">Fonction : </label>
            <select name="fonction" id="fonction">
                <?php $listeFonction = $fonctionManager->getListFonction(); 
                    foreach($listeFonction as $fonction) { ?>
                <option value="<?php echo $fonction->getFon_num(); ?>"><?php echo $fonction->getFon_libelle(); ?></option>
                <?php } ?>
            </select>
       </div>
        <input class="pure-button pure-button-primary" type="submit" name="valider" value="Valider">
    </fieldset>
</form>