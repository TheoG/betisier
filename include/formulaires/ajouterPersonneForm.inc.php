<h1>Ajouter une personne</h1>
   

<form class="pure-form pure-form-aligned" action="#" method="post">
    <fieldset>
        <div class="pure-control-group">
            <label for="nom">Nom : </label>
            <input id="nom" type="text" name="nom" value="" required="required">
        </div>
        <div class="pure-control-group">
            <label for="prenom">Prénom : </label>
            <input id="prenom" type="text" name="prenom" value="" required="required">
        </div>
        <div class="pure-control-group">
            <label for="tel">Téléphone : </label>
            <input id="tel" type="text" name="tel" value="" required="required" pattern="^(?:0|\(?\+33\)?\s?|0033\s?)[1-79](?:[\.\-\s]?\d\d){4}$">
        </div>
        <div class="pure-control-group">
            <label for="mail">e-mail : </label>
            <input id="mail" type="email" name="mail" value="" required="required">
        </div>
        <div class="pure-control-group">
            <label for="login">Login : </label>
            <input id="login" type="text" name="login" value="" required="required">
        </div>
        <div class="pure-control-group">
            <label for="mdp">Mot de passe : </label>
            <input id="mdp" type="password" name="mdp" value="" required="required">
        </div>        
        <div class="pure-control-group">
            <label>Catégorie : </label>
            <input type="radio" name="categorie" value="Personnel" required="required"> Personnel
            <input type="radio" name="categorie" value="Etudiant" required="required"> Etudiant
        </div>
        <input class="pure-button pure-button-primary" type="submit" name="valider" value="Valider">
    </fieldset>
</form>