<?php
    $citationManager = new CitationsManager();
?>

<div id="menu">
	<div id="menuInt">
		<p><a href="index.php?page=<?php echo ACCUEIL ?>"><img class = "icone" src="image/accueil.gif"  alt="Accueil"/>Accueil</a></p>
		<p><img class = "icone" src="image/personne.png" alt="Personne"/>Personne</p>
		<ul>
			<li><a href="index.php?page=<?php echo LISTER_PERSONNE ?>">Lister</a></li>

			<?php if (isset($_SESSION['isConnected'])) { ?>
			    <li><a href="index.php?page=<?php echo AJOUTER_PERSONNE ?>">Ajouter</a></li>
			     <?php if ($_SESSION['isAdmin'] === true) { ?>
			        <li><a href="index.php?page=<?php echo SUPPRIMER_PERSONNE ?>">Supprimer</a></li>
			     <?php } ?>
			<?php } ?>
		</ul>
		<p><img class="icone" src="image/citation.gif"  alt="Citation"/>Citations</p>
		<ul>
			<?php if (isset($_SESSION['isConnected']) && $_SESSION['isAdmin'] === false) { ?>
			    <li><a href="index.php?page=<?php echo AJOUTER_CITATION ?>">Ajouter</a></li>
			<?php } ?>
			<li><a href="index.php?page=<?php echo LISTER_CITATION ?>">Lister</a></li>
			<?php if (isset($_SESSION['isConnected'])) { ?>
			    <li><a href="index.php?page=<?php echo RECHERCHER_CITATION ?>">Rechercher</a></li>
			    
			    <?php if ($_SESSION['isAdmin']) { ?>
			        <li><a href="index.php?page=<?php echo VALIDER_CITATION ?>">Valider (<?php echo $citationManager->getNbCitationsInvalides(); ?>)</a></li>
			        <li><a href="index.php?page=<?php echo SUPPRIMER_CITATION ?>">Supprimer</a></li>
			    <?php } ?>
			<?php } ?>
		</ul>
		<p><img class = "icone" src="image/ville.png" alt="Ville"/>Ville</p>
		<ul>
			<li><a href="index.php?page=<?php echo LISTER_VILLE ?>">Lister</a></li>
            <?php if (isset($_SESSION['isConnected'])) { ?>
			     <li><a href="index.php?page=<?php echo AJOUTER_VILLE ?>">Ajouter</a></li>
			     <li><a href="index.php?page=<?php echo MODIFIER_VILLE ?>">Modifier</a></li>
			    
			     <?php if ($_SESSION['isAdmin']) { ?>
			        <li><a href="index.php?page=<?php echo SUPPRIMER_VILLE ?>">Supprimer</a></li>
			     <?php } ?>
			<?php } ?>
		</ul>
	</div>
</div>
