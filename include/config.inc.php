<?php
// Paramètres de l'application Betisier de l'IUT
// A modifier en fonction de la configuration

define('DBHOST', "localhost");
define('DBNAME', "betisier");
define('DBUSER', "bd");
define('DBPASSWD', "bede");
define('ENV','dev');
// J'ai laissé l'environnement en dev pour votre correction de l'exerice

define('ACCUEIL', 0);
define('AJOUTER_PERSONNE', 1);
define('LISTER_PERSONNE', 2);
define('MODIFIER_PERSONNE', 3);
define('SUPPRIMER_PERSONNE', 4);
define('AJOUTER_CITATION', 5);
define('LISTER_CITATION', 6);
define('AJOUTER_VILLE', 7);
define('LISTER_VILLE', 8);
define('SE_CONNECTER', 9);
define ('DECONNEXION', 10);
define('MODIFIER_VILLE', 11);
define('SUPPRIMER_VILLE', 12);
define('RECHERCHER_CITATION', 13);
define ('VALIDER_CITATION', 14);
define('SUPPRIMER_CITATION', 15);
define('ERREUR', 16);

define('SEL', "48@!alsd");

?>
