<!doctype html>
<html lang="fr">
    <head>
        <meta charset="utf-8">
        <?php
        $title = "Bienvenue sur le bêtisier !"; ?>
        <title>
            <?php echo $title ?>
        </title>
        
        <link rel="icon" type="image/png" href="image/favicon.png" />
        
        <link rel="stylesheet" type="text/css" href="css/stylesheet.css" />
        <link rel="stylesheet" href="js/jquery-ui/jquery-ui.min.css">
        <link rel="stylesheet" href="css/pure-css/base.css">
        <link rel="stylesheet" href="css/pure-css/forms.css">
        <link rel="stylesheet" href="css/pure-css/tables.css">
        <link rel="stylesheet" href="css/pure-css/buttons.css">
        
        <script src="js/jquery-ui/jquery.js" ></script>
        <script src="js/jquery-ui/jquery-ui.js" ></script>
        <script src="js/sorttable.js"></script>
        <script src="js/datepicker-fr.js"></script>
        <script src="js/functions.js"></script>

    </head>
    <body>
        <div id="header">
            <div id="connect">
                <?php if(!isset($_SESSION['isConnected'])) { ?>
                <a href="index.php?page=<?php echo SE_CONNECTER ?>">
                    <p id="pConnexion">Connexion</p>
                </a>
                <?php } else { ?>
                <p>
                    Utilisateur : <?php echo $_SESSION['login'] ?>
                    <a href="index.php?page=<?php echo DECONNEXION ?>"> - Déconnexion</a>
                </p>
                <?php } ?>
            </div>
            <div id="entete">
                <div id="logo">
                    <a href="index.php"><img class="imgLogo" src="image/demanderAVotreGrandMereDeVousEnAcheterUn.png" alt="GrandMere"></a>
                </div>
                <div id="titre">
                    Le bêtisier de l'IUT,<br />Partagez vos meilleures perles !
                </div>
            </div>
        </div>
