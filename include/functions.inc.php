<?php
	function getEnglishDate($date){
		$membres = explode('/', $date);
		$date = $membres[2].'-'.$membres[1].'-'.$membres[0];
		return $date;
	}

    function getFrenchDate($date) {
        $membres = explode('-', $date);
		$date = $membres[2].'/'.$membres[1].'/'.$membres[0];
		return $date;
    }
	
    function redirige($secondes) {
        header('refresh:'. $secondes. ';  url=index.php?page='.ACCUEIL);
    }

    function redirigeIndex($secondes, $localisation) {
        header('refresh:'. $secondes. '; url='.$localisation);
    }

    function isDate($date) {
		$membres = explode('/', $date);
        
        if (count($membres) === 3) {
            return true;
        }
        else {
            return false;
        }
    }

    function erreur() {
        header("Location: index.php?page=".ERREUR); 
    }

    function testAccesDisconnected() {
        if (!isset($_SESSION['isConnected'])) {
            erreur();
        }
    }

    function testAccessAdmin() {
        if (isset($_SESSION['isAdmin']) && $_SESSION['isAdmin'] == false) {
            erreur();
        }
    }
?>